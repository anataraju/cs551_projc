TOPDIR = .
TIMERS = ./timers
CC = gcc
CXX = g++
CPPFLAGS  = 	-I. -g -Wall -fno-inline
CFLAGS    = 	-I. -g -Wall -fno-inline

FLAGS     = 	${CPPFLAGS} ${AC_DEFS}

TIMERSC_LIB_OBJS = ${TIMERS}/timers.o ${TIMERS}/timers-c.o ${TIMERS}/tools.o
PROJC_LIB_OBJS = ${TOPDIR}/projc.o ${TOPDIR}/manager.o ${TOPDIR}/client.o ${TOPDIR}/comm.o ${TOPDIR}/sha1.o ${TOPDIR}/log.o ${TOPDIR}/ring.o
all: projc test-app-c

projc: $(PROJC_LIB_OBJS) $(TIMERSC_LIB_OBJS)
	$(CXX) -Wall $(PROJC_LIB_OBJS) $(TIMERSC_LIB_OBJS) -o $(TOPDIR)/projc

${TOPDIR}/projc.o: $(TOPDIR)/projc.c $(TOPDIR)/projc.h
	$(CC) $(FLAGS) -c $(TOPDIR)/projc.c

${TOPDIR}/manager.o: $(TOPDIR)/manager.c $(TOPDIR)/manager.h
	$(CC) $(FLAGS) -c $(TOPDIR)/manager.c

${TOPDIR}/client.o: $(TOPDIR)/client.c $(TOPDIR)/client.h 
	$(CC) $(FLAGS) -c $(TOPDIR)/client.c

${TOPDIR}/comm.o: $(TOPDIR)/comm.h $(TOPDIR)/comm.c
	$(CC) $(FLAGS) -c $(TOPDIR)/comm.c

${TOPDIR}/sha1.o: $(TOPDIR)/sha1.h $(TOPDIR)/sha1.c
	$(CC) $(FLAGS) -c $(TOPDIR)/sha1.c

${TOPDIR}/log.o: $(TOPDIR)/log.h $(TOPDIR)/log.c
	$(CC) $(FLAGS) -c $(TOPDIR)/log.c

${TOPDIR}/ring.o: $(TOPDIR)/ring.h $(TOPDIR)/ring.c
	$(CC) $(FLAGS) -c $(TOPDIR)/ring.c

${TIMERS}/tools.o: ${TIMERS}/tools.cc ${TIMERS}/tools.hh
	$(CXX) $(FLAGS) -c ${TIMERS}/tools.cc
	mv ./tools.o ${TIMERS}

${TIMERS}/timers.o: ${TIMERS}/timers.cc ${TIMERS}/timers.hh
	$(CXX) $(FLAGS) -c ${TIMERS}/timers.cc
	mv ./timers.o ${TIMERS}

${TIMERS}/timers-c.o: ${TIMERS}/timers-c.cc ${TIMERS}/timers-c.h
	$(CXX) $(FLAGS) -c ${TIMERS}/timers-c.cc
	mv ./timers-c.o ${TIMERS}

test-app-c: $(TIMERSC_LIB_OBJS) ${TIMERS}/test-app-c.o
	$(CXX) $(FLAGS) $(TIMERSC_LIB_OBJS) ${TIMERS}/test-app-c.o -o ${TOPDIR}/test-app-c

clean:
	rm -rf ${TOPDIR}/projc ${TOPDIR}/test-app-c
	rm -rf ${TOPDIR}/*.o ${TIMERS}/*.o
