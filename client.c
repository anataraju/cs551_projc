/*
 * This code "USC CSci551 Projects A and B FA2011" is
 * Copyright (C) 2011 by Xun Fan.
 * All rights reserved.
 *
 * This program is released ONLY for the purposes of Fall 2011 CSci551
 * students who wish to use it as part of their Project C assignment.
 * Use for another other purpose requires prior written approval by
 * Xun Fan.
 *
 * Use in CSci551 is permitted only provided that ALL copyright notices
 * are maintained and that this code is distinguished from new
 * (student-added) code as much as possible.  We new services to be
 * placed in separate (new) files as much as possible.  If you add
 * significant code to existing files, identify your new code with
 * comments.
 *
 * As per class assignments, use of any code OTHER than this provided
 * code requires explicit approval, ahead of time, by the professor.
 *
 */

// File name:   client.c
// Author:    Xun Fan (xunfan@usc.edu)
// Date:    2011.8
// Description: CSCI551 Fall 2011 project b, client module source file.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include "timers/timers-c.h"
#include "projc.h"
#include "client.h"
#include "sha1.h"
#include "comm.h"
#include "log.h"
#include "ring.h"

unsigned int    HashID;// my hash id
unsigned int    HashIDfirst;   // first node hash id
char            Myname[MAX_CLIENT_NAME_SIZE] = {0};  // Triad string name of self
unsigned short  MyUDPPort;  // my udp port
unsigned short  FirstNodePort; // first created node port
unsigned int    TKCC = 0;
char            FirstNodeName[MAX_CLIENT_NAME_SIZE] = {0};
unsigned int    nMgrNonce = 0;

pCStore CStoreHead = NULL;  // client stored string stack head
int nCStore = 0;

FTNODE  MyFT[FTLEN];  // finger table
TNode succ; // successor node
TNode pred; // predecessor node
TNode dsuc; // successor's successor node
TNode dpre; // predecessor's predecessor node


char logfilename[256];

enum query_state { HELLOQ_SENT = 1, HELLOR_RECVD };
int   HELLO_STATE = 0;
int   TCPSock;
int CLIENT_HAS_BEEN_KILLED = 0;
int timer_handle = 0;
int EXIT_END_MSG = 9999;

//
// XXX Read and understand this function first!
// 
// This instantiates the client and contains the main select loop for
// handling all the commands sent to the client from other clients
// or the manager.
//
// The client also exits gracefully when the manager process exits.
//
// XXX There are some debugging messages that might be helpful in
// understanding the code; most of these have been commented out.
// Do a search through this file for "debug".
//
int client(int mgrport)
{
  int nPid;
  int nMgrport;
  
  // get manager port number
  nMgrport = mgrport;
    
  // get pid
  nPid = getpid();

  //printf("worker pid: %d manager port:%d\n", nPid, nMgrport);
  
  int   nSockwkr, udpSock;//,nBytesnum
  socklen_t nSinsize;
  struct  sockaddr_in sinWkraddr, sinWkraddrudp, sinWkrcopy;
// char  szRecvbuf[128];
  char  szSendbuf[128];
// int   nRecvbyte;
  int nSendlen;//, nRemainlen;//, nSent;
  //int   nRecvbufsize, nWrtpos;
  int   nSum;
  //char  szWritebuf[256];
  
  
  fd_set  readset;
  fd_set  allset;

  // Create socket
  if ((nSockwkr = socket(AF_INET, SOCK_STREAM, 0)) == -1){
    printf("projc worker %d error: create socket error! Exit!\n", nPid);
    return -1;
  }

  // initiate address
  sinWkraddr.sin_family = AF_INET;
  sinWkraddr.sin_port = htons(nMgrport);  //manager port
  
  // connect to local address
  sinWkraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  bzero(&(sinWkraddr.sin_zero), 8);
  
  nSinsize = sizeof(struct sockaddr_in);
  if (connect(nSockwkr, (struct sockaddr *)&sinWkraddr, nSinsize) < 0)
  {
    printf("projc worker %d error: connect error! Exit!\n", nPid);
    return -1;
  }
  
  // first join the Triad ring, no need to use select
  
  // get initial information
  if (GetInitInfo(nSockwkr, Myname, FirstNodeName, &nMgrNonce, &FirstNodePort, &TKCC) < 0){
    errexit("client recv initial informatioin fail!\n");
  }
  
  // build logfilename
  snprintf(logfilename, sizeof(logfilename), "stage%d.%s.out", nStage, Myname);
  logfileinit(logfilename);
  
  printf("client: nonce %d, myname %s, firstNName %s, firstNPort %d, TKCC %d\n", nMgrNonce, Myname, FirstNodeName, FirstNodePort, TKCC);
  

  // create udp socket
  if ((udpSock = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
    printf("projc worker %d error: create udp socket error! Exit!\n", nPid);
    return -1;
  }
  
  sinWkraddrudp.sin_family = AF_INET;
  sinWkraddrudp.sin_port = htons(0); // Random port
  sinWkraddrudp.sin_addr.s_addr = inet_addr("127.0.0.1");
  bzero(&(sinWkraddrudp.sin_zero), 8);
  
  // bind to udp socket
  if (bind(udpSock, (struct sockaddr *)&sinWkraddrudp, sizeof(sinWkraddrudp)) < 0){
    printf("projc worker %d error: udp socket bind error! Exit!\n", nPid);
    close(nSockwkr);
    return -1;
  }

  // get udp port number
  nSinsize = sizeof(struct sockaddr_in);
  if (getsockname(udpSock, (struct sockaddr *)&sinWkrcopy, &nSinsize) < 0){
    printf("projc worker %d error: udp socket getsockname error! Exit!\n", nPid);
    return -1;
  }
  MyUDPPort = ntohs(sinWkrcopy.sin_port);
  
  // Triad: calculate ID, join ring
  HashID = gethashid(nMgrNonce, Myname);
  HashIDfirst = gethashid(nMgrNonce, FirstNodeName);
  
  // debug message
  printf("client %d has name %s (%08x) with port %d\n", nPid, Myname, HashID, MyUDPPort);
  
  if (nStage < 4) {
    // stage < 4: join the ring
    if (JoinRing(udpSock) < 0) {
      printf("projc client %s error: join ring error! Exit!\n", Myname);
      close(nSockwkr);
      close(udpSock);
      return -1;
    }
  } else {
    // stage >= 4: we need a finger table
    InitFingerTableSelf();
    // join ring and initiate finger table
    if (JoinRingWithFingerTable(udpSock) < 0) {
      printf("projc client %s error: join ring error! Exit!\n", Myname);
      close(nSockwkr);
      close(udpSock);
      return -1;
    
    }
  }
  
  // send information back to manager
  nSum = nMgrNonce + nPid;
  snprintf(szSendbuf, sizeof(szSendbuf), "%d %d\n%d\n", nSum, nPid, MyUDPPort);

  nSendlen = strlen(szSendbuf);
  if (SendStreamData(nSockwkr, szSendbuf, nSendlen) < 0){
    printf("projc worker %d error: send back to manager error! Exit!\n", nPid);
    return -1;
  }
  
  // lets set up the timers
  static struct TestTimer1_context tt1_ctx;
  
  tt1_ctx.count = 0;
  tt1_ctx.sock = udpSock;

  //
  if (nStage == 6)
    timer_handle = Timers_AddTimer(10000, TestTimer1_expire, (void*)&tt1_ctx);

  int sockMax;
  
  TCPSock = nSockwkr;

  FD_ZERO(&allset);
  FD_SET(nSockwkr, &allset);
  sockMax = nSockwkr;
  
  FD_SET(udpSock, &allset);
  if (udpSock > sockMax) {  // reset the maximum socket
    sockMax = udpSock;
  }
  
  struct timeval tmv;
  tmv.tv_sec = SELECT_TIMEOUT; 
  tmv.tv_usec = 0;
  int status;

  
  // main select loop
  while(1) {

    readset = allset;
    
    if (nStage == 6) {
      Timers_NextTimerTime(&tmv);
      if (tmv.tv_sec == 0 && tmv.tv_usec == 0) {
          // No Timer have been defined 
          Timers_ExecuteNextTimer();
          continue;
      }
      if (tmv.tv_sec == MAXVALUE && tmv.tv_usec == 0){
        /* There are no timers in the event queue */
              break;
      }
    }

    status = select(sockMax+1, &readset, NULL, NULL, &tmv);
    if (status < 0) {
      errexit("client select.\n");
    }

    else if (status > 0) { 
      /*
       * The socket has received data.
       * Perform packet processing.
       */
      // handle messages from other clients over UDP
      if (FD_ISSET(udpSock, &readset)) {
      //  printf("clinet %s (%08x %d) got something on UDP\n", Myname, HashID, MyUDPPort);
        if (CLIENT_HAS_BEEN_KILLED == 0) {
          if (HandleUdpMessage(udpSock) < 0){
            printf("projc client %s, HandleUdpMessage fails!\n", Myname);
            return -1;
          }
        }
      }
     
      // handle messages from the manager over TCP (and graceful exiting)
      if (FD_ISSET(nSockwkr, &readset)) {  // manager sock
        int retval = HandleTCPMessage(nSockwkr, udpSock);
        if (retval < 0)
        {
          printf("projc client %s, HandleTcpMessage fails!\n", Myname);
          return -1;
        }
        if (retval == EXIT_END_MSG)
        {
          break;
        }
      }
    } // end manager socket handling
    else if (status == 0) {
      if (nStage == 6)
      {  /* Timer expired, Hence process it  */
            Timers_ExecuteNextTimer();
        /* Execute all timers that have expired.*/
        Timers_NextTimerTime(&tmv);
        while(tmv.tv_sec == 0 && tmv.tv_usec == 0) {
          /* Timer at the head of the queue has expired  */
          Timers_ExecuteNextTimer();
          Timers_NextTimerTime(&tmv);
        }
      }
    }
  } // end select loop
  
  fflush(stdout);
  close(nSockwkr);
  close(udpSock);
  
  return 0;
}

int HandleTCPMessage(int sock, int udpSock) {
  // mechanism for process exit: look at parent socket
  //printf("projc client %s, got sth from manager\n", Myname);
  char szRecvbuf[128];
  int nRecvbyte = 0;
  char  sstr[MAX_TEXT_SIZE] = {0};
  int nPid = getpid();

  memset(szRecvbuf, 0, sizeof(szRecvbuf));

  if ((nRecvbyte = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0)
  {
    // if parent exits, then we should exit too

    if (nRecvbyte == 0)
      printf("projc client %s: parent socket close, exit!\n", Myname);
    else
      printf("projc client %s: parent socket recv error, exit!\n", Myname);
    
    if (nStage == 6)
      Timers_RemoveTimer(timer_handle);
    return EXIT_END_MSG;
  }
  else {   // jobs assignment
    szRecvbuf[nRecvbyte-1] = '\0';
    if (strcmp(szRecvbuf, "exit!") == 0){
      printf("projc client %s recv EXIT from manager!\n", Myname);

      // debug: log finger table
      
      /*if (nStage >= 4){
        LogFingerTable();
      }*/
      
      //if (CLIENT_HAS_BEEN_KILLED == 0)
      if (nStage == 6)
        Timers_RemoveTimer(timer_handle);

      return EXIT_END_MSG;
    }
    else if(strcmp(szRecvbuf, "store") == 0) {
      // handle store command
      memset(sstr, 0, sizeof(sstr));
      if ((nRecvbyte = RecvStreamLineForSelect(sock, sstr, sizeof(sstr))) <= 0){
        printf("projc client %s error: recv store string from manager!\n", Myname);
        return -1;
      }
      sstr[nRecvbyte-1] = '\0';
      
      if (nStage == 7) {
        // Amogh - case added
        if (HandleStoreMsgStage7(udpSock, sstr) < 0 ){
          printf("projc client %s error: handle store msg from manager!\n", Myname);
          return -1;
        }
      }
      else {
        if  (HandleStoreMsg(udpSock, sstr) < 0 ){
          printf("projc client %s error: handle store msg from manager!\n", Myname);
          return -1;
        }
      }
      // printf("%s: time to tell manager ok\n", Myname);
    }
    else if(strcmp(szRecvbuf, "search") == 0) {
      // handle search command  
      memset(sstr, 0, sizeof(sstr));
      if ((nRecvbyte = RecvStreamLineForSelect(sock, sstr, sizeof(sstr))) <= 0){
        printf("projc client %s error: recv store string from manager!\n", Myname);
        return -1;
      }
      sstr[nRecvbyte-1] = '\0';
      if (nStage == 7) {
        if (HandleSearchMsgStage7(udpSock, sstr) < 0 ){
          printf("projc client %s error: handle search msg from manager!\n", Myname);
          return -1;
        }
      }

      else {
        if (HandleSearchMsg(udpSock, sstr) < 0 ){
          printf("projc client %s error: handle store msg from manager!\n", Myname);
          return -1;
        }
      }
    }
    else if(strcmp(szRecvbuf, "end_client") == 0) {
      // handle end_client command
      memset(sstr, 0, sizeof(sstr));
      if ((nRecvbyte = RecvStreamLineForSelect(sock, sstr, sizeof(sstr))) <= 0){
        printf("projc client %s error: recv end_client from manager!\n", Myname);
        return -1;
      }
      sstr[nRecvbyte-1] = '\0';

      // sanity check
      if (strcmp(sstr, Myname) != 0){
        printf("projc client %s error: target of end_client is not me! name: %s\n", Myname, sstr);
        return -1;
      }

      if (HandleEndClient(udpSock) < 0){
        printf("projc client %s error: handle end_client msg from manager!\n", Myname);
        return -1;
      }
    } 
    else if (strcmp(szRecvbuf, "kill_client") == 0) {
      //handle kill_client command
      memset(sstr, 0, sizeof(sstr));
      if ((nRecvbyte = RecvStreamLineForSelect(sock, sstr, sizeof(sstr))) <= 0){
        printf("projc client %s error: recv kill_client from manager!\n", Myname);
        return -1;
      }
      sstr[nRecvbyte-1] = '\0';

      // sanity check
      if (strcmp(sstr, Myname) != 0){
        printf("projc client %s error: target of kill_client is not me! name: %s\n", Myname, sstr);
        return -1;
      }
      CLIENT_HAS_BEEN_KILLED = 1;
      sleep(60); // don't respond to any messages but wait until the ring stabilizes
    } // end handling of commands
    
    // tell manger ok

    char  szSendbuf[128];
    int nSendlen = 0;
    snprintf(szSendbuf, sizeof(szSendbuf), "ok\n");
    nSendlen = strlen(szSendbuf);
    
    if (SendStreamData(sock, szSendbuf, nSendlen) < 0){
      printf("projc worker %d error: send ok back to manager error! Exit!\n", nPid);
      return -1;
    }

  } // end jobs assignment
  return 0;
}

int TestTimer1_expire(void *context)
{
    /* demonstrate passing a structure with arbitrary context in it */
    struct TestTimer1_context *tt1_ctx = (struct TestTimer1_context *)context;
    struct timeval tv;
    
    if (!CLIENT_HAS_BEEN_KILLED) {
      tt1_ctx->count++;

      getTime(&tv);
      /*fprintf(stderr, "projc client %s: Timer 1 has expired %d times! Time %d.%06d\n",
            Myname, tt1_ctx->count, (int)tv.tv_sec, (int)tv.tv_usec);
      */
      SendHelloPredQuery(tt1_ctx->sock);
      
      fflush(NULL);
    }
    return 0;
}

int GetInitInfo(int sock, char *selfname, char *firstnode, unsigned int *nonce, unsigned short *port, unsigned int *ptkcc){
  char szRecvbuf[256] = {0};
  int nRecvsize = 0;
  int nc;
  int p;
  
  // get nonce
  if ((nRecvsize = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0){
    return -1;
  }
  szRecvbuf[nRecvsize-1] = '\0'; // turn \n to \0
  nc = atoi(szRecvbuf);
  *nonce = (unsigned int)nc;
  
  // get selfname
  if ((nRecvsize = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0){
    return -1;
  }
  szRecvbuf[nRecvsize-1] = '\0'; // turn \n to \0
  strncpy(selfname, szRecvbuf, MAX_CLIENT_NAME_SIZE);
  
  // get first node udp port
  if ((nRecvsize = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0){
    return -1;
  }
  szRecvbuf[nRecvsize-1] = '\0'; // turn \n to \0
  if (strlen(szRecvbuf) == 1 && strcmp(szRecvbuf, "0") == 1){ // first node
    *port = 0;
  }else{
    p = atoi(szRecvbuf);
    *port = (unsigned short)p;
  }
  
  // get first node name
  if ((nRecvsize = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0){
    return -1;
  }
  szRecvbuf[nRecvsize-1] = '\0'; // turn \n to \0
  strncpy(firstnode, szRecvbuf, MAX_CLIENT_NAME_SIZE);

  if ((nRecvsize = RecvStreamLineForSelect(sock, szRecvbuf, sizeof(szRecvbuf))) <= 0){
    return -1;
  }
  szRecvbuf[nRecvsize-1] = '\0';
  p = atoi(szRecvbuf);
  *ptkcc = (unsigned int)p;

  return 0;
}

int JoinRing(int sock){
  TNode TempA, TempB;
  char  wbuf[128];
  int   msgtype;
  int flag = 0;
 
  // are we the first node of the ring?
  if (HashID == HashIDfirst) {
    succ.id = HashID;
    succ.port = MyUDPPort;
    pred.id = HashID;
    pred.port = MyUDPPort;
  }
  else {
    // we are not the first node
    if (HashID < HashIDfirst){ // counterclockwise
      msgtype = PREDQ;
      TempA.id = HashIDfirst;
      TempA.port = FirstNodePort;
      while(flag == 0){
        if (FindNeighbor(sock, msgtype, TempA, &TempB) < 0){ // errexit
          return -1;
        }
        // two situation to set flag
        // 1. B.hash >= A.hash
        // 2. HashID < A.hash && HashID > B.hash
        if (TempB.id >= TempA.id){
          flag = 1;
        }else if (HashID > TempB.id){
          flag = 1;
        }else if (HashID < TempB.id){ // A=B, B
          TempA.id = TempB.id;
          TempA.port = TempB.port;
        }
      }
      // tell A to change its predecessor, tell B to change its successor
      if (UpdateNeighbor(sock, &TempA, &TempB) < 0){
        return -1;
      }
      // A is the successor, B is predecessor
      succ.id = TempA.id;
      succ.port = TempA.port;
      pred.id = TempB.id;
      pred.port = TempB.port;
    } // end counterclockwise
    else { //clockwise
      msgtype = SUCCQ;
      TempA.id = HashIDfirst;
      TempA.port = FirstNodePort;
      
      while(flag == 0){
        if (FindNeighbor(sock, msgtype, TempA, &TempB) < 0){ // errexit
          return -1;
        }
        // two situation to set flag
        // 1. B.hash <= A.hash
        // 2.  HashID < B.hash
        if (TempB.id <= TempA.id){
          flag = 1;
        }else if (HashID < TempB.id){
          flag = 1;
        }else if (HashID > TempB.id){ // set A to B
          TempA.id = TempB.id;
          TempA.port = TempB.port;
        }
      }
      // tell B to change its predecessor, tell A to change its successor
      if (UpdateNeighbor(sock, &TempB, &TempA) < 0){
        return -1;
      }
      
      succ.id = TempB.id;
      succ.port = TempB.port;
      pred.id = TempA.id;
      pred.port = TempA.port;
    } // end clockwise
  } // end joining of the ring
  
  // join finishes, write to log
  snprintf(wbuf, sizeof(wbuf), "client %s created with hash 0x%08x\n", Myname, HashID);
  logfilewriteline(logfilename, wbuf, strlen(wbuf));
  
  return 0;
}

int JoinRingWithFingerTable(int sock){
  char  wbuf[128];
  
  if (HashID != HashIDfirst){ // not first node
    if (InitFingerTable(sock) < 0){
      printf("projc client %s: InitFingerTable fails!\n", Myname);
      return -1;
    }

    /**************** for debug*/
    //LogFingerTable();
    
    if (UpdateOthers(sock) < 0){
      printf("projc client %s: UpdateOthers fails!\n", Myname);
      return -1;
    }
  }

  // join finishes, write log
  snprintf(wbuf, sizeof(wbuf), "client %s created with hash 0x%08x\n", Myname, HashID);
  logfilewriteline(logfilename, wbuf, strlen(wbuf));
  
  return 0;
}

int InitFingerTable(int sock){
  TNode mysucc;
  TNode mypred;
  TNode mydsuc;
  TNode mydpre;
  int i;

  // get my successor
  if (FindSuccWithFT(sock, HashID, &mysucc) < 0){
    printf("projc client %s: InitFingerTable->FindSuccWithFT find successor fails!\n", Myname);
    return -1;
  }
  // set my successor
  succ.id = mysucc.id;
  succ.port = mysucc.port;

  MyFT[1].node.id = mysucc.id;
  MyFT[1].node.port = mysucc.port;

  // get my predecessor
  if (FindNeighbor(sock, PREDQ, mysucc, &mypred) < 0){
    printf("projc client %s: InitFingerTable->FindNeighbor find predecessor fails!\n", Myname);
    return -1;
  }
  // set my predecessor
  pred.id = mypred.id;
  pred.port = mypred.port;
  MyFT[0].node.id = mypred.id;
  MyFT[0].node.port = mypred.port;

  // Amogh
  // get my double successor
  if (FindNeighbor(sock, SUCCQ, mysucc, &mydsuc) < 0){
    printf("projc client %s: InitFingerTable->FindNeighbor find double successor fails!\n", Myname);
    return -1;
  }
  // set my double successor
  dsuc.id = mydsuc.id;
  dsuc.port = mydsuc.port;

  // get my double predecessor, if we don't maintain will be trouble while leaving
  if (FindNeighbor(sock, PREDQ, mypred, &mydpre) < 0){
    printf("projc client %s: InitFingerTable->FindNeighbor find double predecessor fails!\n", Myname);
    return -1;
  }
  dpre.id = mydpre.id;
  dpre.port = mydpre.port;

  // update my finger table
  // note that now I am not a node in the ring, so this round of initiate may not correct,
  // will update my finger table later.
  for (i=1; i<(FTLEN-1); i++){
    if (InRangeA(HashID, MyFT[i+1].start, MyFT[i].node.id)){
      MyFT[i+1].node.id = MyFT[i].node.id;
      MyFT[i+1].node.port = MyFT[i].node.port;
    }else{
      //find succ
      TNode tn;
      if (FindSuccWithFT(sock, MyFT[i+1].start, &tn) <  0){
        printf("projc client %s: InitFingerTable->FindSuccWithFT(in loop)  fails! \n", Myname);
        return -1;
      }
      MyFT[i+1].node.id = tn.id;
      MyFT[i+1].node.port = tn.port;
    }
  }

  //update my neighbours about me
  if (UpdateNeighborNew(sock, &mysucc, &mypred, &mydpre, &mydsuc) < 0){
    printf("projc client %s: InitFingerTable->UpdateNeighborNew fails! \n", Myname);
    return -1;
  }

  if (UpdateCriscross(sock) < 0) {
    printf("projc client %s: InitFingerTable->UpdateCriscross fails! \n", Myname);
    return -1;
  }

  // now update my finger table
  // just look at those finger table entries whose node is my successor
  UpdateMyFingerTableInit();

  return 0;
}

void UpdateMyFingerTableInit(){
  int i;
  for (i=1; i<FTLEN; i++){
    if (MyFT[i].node.id == succ.id){
      if (NotInRange(MyFT[i].start, HashID, succ.id)){
        MyFT[i].node.id = HashID;
        MyFT[i].node.port = MyUDPPort;
      }
    }
  }
}

int UpdateOthers(int sock){
  int i;
  unsigned int nTemp;
  unsigned int exp;
  TNode tempsu;
  TNode temppr;
  TNode myself;

  for (i=1; i<FTLEN; i++){
    // circle minus
    exp = (unsigned int)(1<<(i-1));

    nTemp = RingMinus(HashID, exp);
    /*if (n >= exp){
      nTemp = n - exp;
    }
    else{
      nTemp = HASHMAX - (exp - n -1);
    }*/

    // find predecessor => find successor and find successor's predecessor
    if(FindSuccWithFT(sock, nTemp, &tempsu) < 0){
      printf("projc client %s: UpdateOthers->FindSuccWithFT fails!\n", Myname);
      return -1;
    }

    /**************** for debug **********
    char wbuf[128];
    snprintf(wbuf, sizeof(wbuf), "UpdateOthers find succ of %08x, succ is %08x \n", nTemp, tempsu.id);
    logfilewriteline(logfilename, wbuf, strlen(wbuf));
    **********/

    if (FindNeighbor(sock, PREDQ, tempsu, &temppr) < 0){
      printf("projc client %s: UpdateOthers->FindNeighbor find predecessor fails!\n", Myname);
      return -1;
    }

    /**************** for debug **********
    snprintf(wbuf, sizeof(wbuf), "UpdateOthers find pred of %08x, pred is %08x \n", nTemp, temppr.id);
    logfilewriteline(logfilename, wbuf, strlen(wbuf));
    *********/

    myself.id = HashID;
    myself.port = MyUDPPort;

    // if it's myself
    if(temppr.id == HashID){
      /*** never update myself in update others ****
      **********************************************
      if (UpdateMyFingerTable(sock, myself, i) < 0){
        printf("projc client %s: UpdateOthers->UpdateMyFingerTable fails!\n", Myname);
        return -1;
      }************/
      //
      continue;
    }

    
    if (UpdateFingerTable(sock, temppr, myself, i) < 0){
      printf("projc client %s: UpdateOthers->UpdateFingerTable 0x%08x %d fails!\n", Myname, temppr.id, i);
      return -1;
    }
    // if other nodes, do it iteratively
    /*
    tempFstart = RingPlus(temppr.id, exp);
    while(!NotInRange(tempFstart, pred.id, HashID)){
      if (UpdateFingerTable(sock, temppr, i) < 0){
        printf("projc client %s: UpdateOthers->UpdateFingerTable 0x%08x %d fails!\n", Myname, temppr.id, i);
        return -1;
      }

      // get temppr's predecessor
      if (FindNeighbor(sock, PREDQ, temppr, &temppr2) < 0){
        printf("projc client %s: UpdateOthers->FindNeighbor in loop find predecessor fails!\n", Myname);
        return -1;
      }

      temppr.id = temppr2.id;
      temppr.port = temppr2.port;

      tempFstart = RingPlus(temppr.id, exp);
    }*/
  }
  return 0;
}

void InitFingerTableSelf(){
  unsigned int diff;
  int i;
  
  unsigned int fstr;
  unsigned int fend;
  unsigned int itv;
  
  // init predecessor
  MyFT[0].start = HashID;
  MyFT[0].end = HashID;
  MyFT[0].node.id = HashID;
  MyFT[0].node.port = MyUDPPort;

  succ.id = HashID;
  succ.port = MyUDPPort;
  pred.id = HashID;
  pred.port = MyUDPPort;
  dpre.id = dsuc.id = HashID;
  dpre.port = dsuc.port = MyUDPPort;

  
  // start from 1, init FT
  for (i=1; i<FTLEN; i++){
    itv = (1 << (i-1));
    diff = HASHMAX - HashID;
    
    // int overflow free
    if (diff < itv){
      fstr = itv - diff - 1;
    }else{
      fstr = HashID + itv;
    }
    
    if (i == FTLEN-1){
      fend = HashID;
    }else{
      itv = (1<<i);
      diff = HASHMAX - HashID;
      if (diff < itv){
        fend = itv - diff - 1;
      }else{
        fend = HashID + itv;
      }
    }
    MyFT[i].start = fstr;
    MyFT[i].end = fend;
    MyFT[i].node.id = HashID;
    MyFT[i].node.port = MyUDPPort;
  }
}

int FindNeighbor(int sock, int msgtype, TNode na, TNode *pnb){
  NGQM  qrymsg;
  NGRM  *pngr;
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  //char  writebuf[256];
  int nSendbytes;
  int nRecvbytes;

  // prevent to send self query message
  if (na.id == HashID){
    if (msgtype == SUCCQ){
      pnb->id = succ.id;
      pnb->port = succ.port;
    }
    else if(msgtype == PREDQ){
      pnb->id = pred.id;
      pnb->port = pred.port;
    }

    return 0;
  }
  
  qrymsg.msgid = htonl(msgtype);
  qrymsg.ni = htonl(na.id);
  memcpy(sendbuf, &qrymsg, sizeof(NGQM));
  
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(na.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  // to be simple, just assume sendto always send all data required.
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(NGQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(NGQM)){
    printf("projc error: find_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(NGQM));
    return -1;
  }
  
  // wirte log
  LogTyiadMsg(msgtype, SENTFLAG, sendbuf);
  
  //printf("clinet %s (%08x %d) sent successor-q (%08x %d)\n", Myname, HashID, MyUDPPort, na.id, na.port);
  
  // recvfrom, assume it recv all we need
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) ) != sizeof(NGRM)) {
    printf("projc error: FindNeighbor GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(NGRM));
    return -1;
  } 
 
  // wirte log
  LogTyiadMsg((msgtype+1), RECVFLAG, recvbuf);
  
  //parse data
  pngr = (pngrm)recvbuf;
  // sanity check
  if ((msgtype+1) != ntohl(pngr->msgid) || na.id != ntohl(pngr->ni)){
    printf("projc error: find_neighbor recv ngr msg %d %d, ni %08x %08x\n", msgtype, ntohl(pngr->msgid), na.id, ntohl(pngr->ni));
    return -1;
  }
  
  pnb->id = ntohl(pngr->si);
  pnb->port = ntohl(pngr->sp);
  
  return 0;
}

int FindSuccWithFT(int sock, unsigned int id, TNode *retnode){
  TNode tempA;
  TNode tempB;
  // start from first node
  tempA.id = HashIDfirst;
  tempA.port = FirstNodePort;
  
  // find first node successor.
  if (HashID == HashIDfirst){ // I am the first node
    tempB.id = succ.id;
    tempB.port = succ.port;
  }else{
    if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
      printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
      return -1;
    }
  }
  
  while(NotInRange(id, tempA.id, tempB.id)){
    // find closest 
    if (FindClosest(sock, CLSTQ, id, tempA, &tempB)<0){
      printf("projc client %s: FindSuccWithFT->FindClosest fail, tempA 0x%08x id 0x%08x \n", Myname, tempA.id, id);
      return -1;
    }

    // handle weired situation here
    // if A.id == B.id, there are two situation, 
    // 1. id belongs to A, 
    // 2. id belogns to A.successor
    // need some judgment here
    if (tempA.id == tempB.id){
      //find A's successor
      if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
        printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
        return -1;
      }

      if (tempA.id == tempB.id){  // first node, and we are the second node.
        retnode->id = tempA.id;
        retnode->port = tempA.port;
        return 0;
      }

      if (NotInRange(id, tempA.id, tempB.id)){ // A is successor
        retnode->id = tempA.id;
        retnode->port = tempA.port;
        return 0;
      }else{
        retnode->id = tempB.id;
        retnode->port  = tempB.port;
        return 0;
      }
    }

    tempA.id = tempB.id;
    tempA.port = tempB.port;

    //find A's successor
    if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
      printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
      return -1;
    }
  }

  retnode->id = tempB.id;
  retnode->port = tempB.port;
  
  return 0;
}


int UpdateNeighbor(int sock, TNode *chgpreNode, TNode *chgsucNode){

  printf ("This should NOT be hit!!!\n");
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  UPQM  updmsg;
  
  // first change someone's predecessor
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(chgpreNode->port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  
  updmsg.msgid = htonl(UPDTQ);
  updmsg.ni = htonl(chgpreNode->id);
  updmsg.si = htonl(HashID);
  updmsg.sp = htonl((int)MyUDPPort);
  updmsg.i = htonl(0); // change predecessor
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  // only receive reply after stage 4
  if (nStage >= 2){
    // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf))  ) != sizeof(UPRM)) {
      printf("projc error: FindNeighbor GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }

    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  
  // then, update someone's successor
  naaddr.sin_port = htons(chgsucNode->port);
  
  updmsg.ni = htonl(chgsucNode->id);
  updmsg.i = htonl(1); // update successor
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: FindNeighbor GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }

  return 0;
}

// read change pred for node, succ for node, double pred for node, double succ for node
int UpdateNeighborNew(int sock, TNode *chgpreNode, TNode *chgsucNode, TNode *chgdpreNode, TNode *chgdsucNode){
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  UPQM  updmsg;
  
  // first change someone's predecessor
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(chgpreNode->port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  
  updmsg.msgid = htonl(UPDTQ);
  updmsg.ni = htonl(chgpreNode->id);
  updmsg.si = htonl(HashID);
  updmsg.sp = htonl((int)MyUDPPort);
  updmsg.i = htonl(0); // change predecessor
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: UpdateNeighborNew sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  // only receive reply after stage 4
  if (nStage >= 2){
    // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: UpdateNeighborNew recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  
  // then, update someone's successor
  naaddr.sin_port = htons(chgsucNode->port);
  
  updmsg.ni = htonl(chgsucNode->id);
  updmsg.i = htonl(1); // update successor
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: UpdateNeighborNew sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: UpdateNeighborNew recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }

  //if (nStage == 6) {

    // Amogh: later, update someone's double successor
    naaddr.sin_port = htons(chgdpreNode->port);

    updmsg.ni = htonl(chgdpreNode->id);
    updmsg.i = htonl(100); // update double successor
    memset(sendbuf, 0, sizeof(sendbuf));
    memcpy(sendbuf, &updmsg, sizeof(UPQM));

    if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
      printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
      return -1;
    }
    
    // log
    LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

    
      // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
        printf("projc error: update_neighbor recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
        return -1;
      }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }

    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
    

    // Amogh: later, update someone's double predecessor
    naaddr.sin_port = htons(chgdsucNode->port);

    updmsg.ni = htonl(chgdsucNode->id);
    updmsg.i = htonl(200); // update double predecessor
    memset(sendbuf, 0, sizeof(sendbuf));
    memcpy(sendbuf, &updmsg, sizeof(UPQM));

    if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
      printf("projc error: UpdateNeighborNew sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
      return -1;
    }
    
    // log
    LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

    
      // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
        printf("projc error: update_neighbor recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
        return -1;
      }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }

    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
    
    // Amogh: later, update someone's double successor
    naaddr.sin_port = htons(chgdpreNode->port);

    updmsg.ni = htonl(chgdpreNode->id);
    updmsg.i = htonl(100); // update double successor
    memset(sendbuf, 0, sizeof(sendbuf));
    memcpy(sendbuf, &updmsg, sizeof(UPQM));

    if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
      printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
      return -1;
    }
    
    // log
    LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

    
      // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
        printf("projc error: update_neighbor recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
        return -1;
      }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }

    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
    
  //}

  return 0;
}

// successor's double predecesor changes to my predecessor
// predecessor's double successor changes to my successor
int UpdateCriscross(int sock){
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  UPQM  updmsg;
  
  // first change someone's predecessor
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(succ.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  
  updmsg.msgid = htonl(UPDTQ);
  updmsg.ni = htonl(succ.id);
  updmsg.si = htonl(pred.id);
  updmsg.sp = htonl(pred.port);
  updmsg.i = htonl(200); // change double predecessor
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: UpdateNeighborNew sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  // only receive reply after stage 4
  if (nStage >= 2){
    // receive reply
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: UpdateNeighborNew recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  
  // then, update predecessor's double successor
  naaddr.sin_port = htons(pred.port);
  updmsg.ni = htonl(pred.id);
  updmsg.si = htonl(succ.id);
  updmsg.sp = htonl(succ.port);
  updmsg.i = htonl(100); // change double predecessor
  
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: UpdateNeighborNew sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: UpdateNeighborNew recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  return 0;
}

int UpdateFingerTable(int sock, TNode tn, TNode sn, int idx){
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  UPQM  updmsg;
  puprm ptr;
  
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(tn.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  
  updmsg.msgid = htonl(UPDTQ);
  updmsg.ni = htonl(tn.id);
  updmsg.si = htonl(sn.id);
  updmsg.sp = htonl((int)(sn.port));
  updmsg.i = htonl(idx);
  memcpy(sendbuf, &updmsg, sizeof(UPQM));


  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: UpdateFingerTable sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }

  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
    printf("projc error: UpdateFingerTable recvfrom ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
    return -1;
  }*/
  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
    printf("projc error: UpdateNeighborNew GetNextMessage ret %d, should recv %u\n", nRecvbytes, sizeof(UPRM));
    return -1;
  }

  // sanity check 
  ptr = (puprm)recvbuf;
  if (ntohl(ptr->msgid) != UPDTR){
    printf("projc client %s: UpdateFingerTable recvfrom unexpect message %d!\n", Myname, ntohl(ptr->msgid));
    return -1;
  }
  if ((unsigned int)ntohl(ptr->ni) != tn.id || 
    (unsigned int)ntohl(ptr->si) != sn.id ||
    ntohl(ptr->sp) != (int)sn.port || ntohl(ptr->i) != idx){
    printf("projc client %s: UpdateFingerTable recvfrom wrong message: (0x%08x 0x%08x %d %d)\n", 
      Myname, ntohl(ptr->ni), ntohl(ptr->si),  ntohl(ptr->sp),  ntohl(ptr->i));
    return -1;
  }
  // wirte log
  LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);

  return 0;
}

int UpdateMyFingerTable(int sock, TNode s, int idx){

  if (InRangeA(HashID, s.id, MyFT[idx].node.id)){
    MyFT[idx].node.id = s.id;
    MyFT[idx].node.port = s.port;
    
    if (pred.id != s.id){
      // update pred finger
      if (UpdateFingerTable(sock, pred, s, idx) < 0){
        printf("projc client %s: UpdateMyFingerTable update pred fails!\n", Myname);
        return -1;
      }
    }
  }

  return 0;
}

int HandleUdpMessage(int sock){
  struct sockaddr_in  cliaddr;
  //int sa_len = sizeof(cliaddr);
  socklen_t sa_len = sizeof(cliaddr);
  char  recvbuf[256];
  char  sendbuf[256];
  int sendlen;
  int recvlen;
  int msgtype;
  int *tmp;
  pupqm ptr;
  int ret;
  
  
  // first recv 4 bytes to see which type of message
  if ((recvlen = recvfrom(sock, recvbuf, sizeof(recvbuf), 0, (struct sockaddr *)&cliaddr, &sa_len)) < 0){
    printf("projc error: HandleUdpMessage recvfrom faile!\n");
    return -1;
  }
  //printf("clinet %s (%08x %d) recvd msg type\n", Myname, HashID, MyUDPPort);
  tmp = (int *)recvbuf;
  
  msgtype = ntohl(*tmp);
  
  switch(msgtype){
  case SUCCQ:
//    if ((recvlen = recvfrom(sock, recvbuf+sizeof(int), sizeof(int), 0, (struct sockaddr *)&cliaddr, &sa_len)) != sizeof(int)){
//      printf("projc error: HandleUdpMessage recvfrom ret %d, shoulde recv %d\n", recvlen, sizeof(int));
//      return -1;
//    }
    //sanity check
  {
    if(recvlen != sizeof(NGQM)){  
      printf("projc error: clinet %s (%08x %d) recvd successor-q, but length wrong %d, should be %u\n", Myname, HashID, MyUDPPort, recvlen, sizeof(NGQM));
      return -1;
    }
    // printf("clinet %s (%08x %d) recvd successor-q, lenght %d\n", Myname, HashID, MyUDPPort, recvlen);
    // log
    LogTyiadMsg(SUCCQ, RECVFLAG, recvbuf);
    
    NGRM sucrlp;
    sucrlp.msgid = htonl(SUCCR);
    sucrlp.ni = htonl(HashID);
    sucrlp.si = htonl(succ.id);
    sucrlp.sp = htonl(succ.port);
    memcpy(sendbuf, &sucrlp, sizeof(sucrlp));
    if ((sendlen = sendto(sock, sendbuf, sizeof(sucrlp), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(sucrlp)){
      printf("projc error: HandleUdpMessage sendto ret %d, shoulde send %u\n", sendlen, sizeof(sucrlp));
      return -1;
    }
    LogTyiadMsg(SUCCR, SENTFLAG, sendbuf);
  }
    break;
  case PREDQ:
                //printf("clinet %s (%08x %d) recvd predecessor-q, length %d\n", Myname, HashID, MyUDPPort, recvlen);
  //  if ((recvlen = recvfrom(sock, recvbuf+sizeof(int), sizeof(int), 0, (struct sockaddr *)&cliaddr, &sa_len)) != sizeof(int)){
  //    printf("projc error: HandleUdpMessage recvfrom ret %d, shoulde recv %d\n", recvlen, sizeof(int));
  //    return -1;
  //  }
    // log
  {
    LogTyiadMsg(PREDQ, RECVFLAG, recvbuf);
    
    NGRM prcrlp;
    prcrlp.msgid = htonl(PREDR);
    prcrlp.ni = htonl(HashID);
    prcrlp.si = htonl(pred.id);
    prcrlp.sp = htonl(pred.port);
    memcpy(sendbuf, &prcrlp, sizeof(prcrlp));
    if ((sendlen = sendto(sock, sendbuf, sizeof(prcrlp), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(prcrlp)){
      printf("projc error: HandleUdpMessage sendto ret %d, should send %u\n", sendlen, sizeof(prcrlp));
      return -1;
    }
    LogTyiadMsg(PREDR, SENTFLAG, sendbuf);
  }
    break;
  case HPREQ:
  {
    LogTyiadMsg(HPREQ, RECVFLAG, recvbuf);
    
    NGRM prcrlp;
    prcrlp.msgid = htonl(HPRER);
    prcrlp.ni = htonl(HashID);
    prcrlp.si = htonl(pred.id);
    prcrlp.sp = htonl(pred.port);
    memcpy(sendbuf, &prcrlp, sizeof(prcrlp));
    if ((sendlen = sendto(sock, sendbuf, sizeof(prcrlp), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(prcrlp)){
      printf("projc error: HandleUdpMessage sendto ret %d, should send %u\n", sendlen, sizeof(prcrlp));
      return -1;
    }
    LogTyiadMsg(HPRER, SENTFLAG, sendbuf);
  }
    break;
  case HPRER:
  {
    HandleHelloPredReply(recvbuf);
  }
    break;
  case INFDQ:
  {
    HandleInformDeadQuery(sock, recvbuf, &cliaddr);
  }
    break;
  case UPDTQ:
    //printf("clinet %s (%08x %d) recvd update-q, length %d\n", Myname, HashID, MyUDPPort, recvlen);
  //  if ((recvlen = recvfrom(sock, recvbuf+sizeof(int), (sizeof(UPQM)-sizeof(int)), 0, (struct sockaddr *)&cliaddr, &sa_len)) != sizeof(int)){
  //    printf("projc error: HandleUdpMessage recvfrom ret %d, shoulde recv %d\n", recvlen, (sizeof(UPQM)-sizeof(int)));
  //    return -1;
  //  }
    // log
  {
    LogTyiadMsg(UPDTQ, RECVFLAG, recvbuf);
    TNode t;
    int idx;
    
    ptr = (pupqm)recvbuf;
    idx = ntohl(ptr->i);

    if (idx == 0){ //update predecessor
      pred.id = ntohl(ptr->si);
      pred.port = ntohl(ptr->sp);
      if (nStage >= 2){
        MyFT[0].node.id = pred.id;
        MyFT[0].node.port = pred.port;

        // send reply 
        UPRM uprmsg;
        uprmsg.msgid = htonl(UPDTR);
        uprmsg.ni = htonl(HashID);
        uprmsg.r = htonl(1);
        uprmsg.si = ptr->si;
        uprmsg.sp = ptr->sp;
        uprmsg.i = ptr->i;
        memcpy(sendbuf, &uprmsg, sizeof(UPRM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(UPRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(UPRM)){
          printf("projc error: client %s HandleUdpMessage update-r send ret %d, shoulde send %u\n", Myname, sendlen, sizeof(UPRM));
          return -1;
        }
        LogTyiadMsg(UPDTR, SENTFLAG, sendbuf);
      }
    }else if(idx == 1){ // update successor
      succ.id = ntohl(ptr->si);
      succ.port = ntohl(ptr->sp);
      if (nStage >= 2){
        MyFT[1].node.id = succ.id;
        MyFT[1].node.port = succ.port;

        // send reply 
        UPRM uprmsg;
        uprmsg.msgid = htonl(UPDTR);
        uprmsg.ni = htonl(HashID);
        uprmsg.r = htonl(1);
        uprmsg.si = ptr->si;
        uprmsg.sp = ptr->sp;
        uprmsg.i = ptr->i;
        memcpy(sendbuf, &uprmsg, sizeof(UPRM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(UPRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(UPRM)){
          printf("projc error: client %s HandleUdpMessage update-r send ret %d, should send %u\n", Myname, sendlen, sizeof(UPRM));
          return -1;
        }
        LogTyiadMsg(UPDTR, SENTFLAG, sendbuf);
      }
    }else if(idx == 100){ // update double successor
      dsuc.id = ntohl(ptr->si);
      dsuc.port = ntohl(ptr->sp);
      if (nStage >= 2){
        // send reply 
        UPRM uprmsg;
        uprmsg.msgid = htonl(UPDTR);
        uprmsg.ni = htonl(HashID);
        uprmsg.r = htonl(1);
        uprmsg.si = ptr->si;
        uprmsg.sp = ptr->sp;
        uprmsg.i = ptr->i;
        memcpy(sendbuf, &uprmsg, sizeof(UPRM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(UPRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(UPRM)){
          printf("projc error: client %s HandleUdpMessage update-r send ret %d, should send %u\n", Myname, sendlen, sizeof(UPRM));
          return -1;
        }
        LogTyiadMsg(UPDTR, SENTFLAG, sendbuf);
      }
    }else if(idx == 200){ // update double predecessor
      dpre.id = ntohl(ptr->si);
      dpre.port = ntohl(ptr->sp);
      if (nStage >= 2){
        // send reply 
        UPRM uprmsg;
        uprmsg.msgid = htonl(UPDTR);
        uprmsg.ni = htonl(HashID);
        uprmsg.r = htonl(1);
        uprmsg.si = ptr->si;
        uprmsg.sp = ptr->sp;
        uprmsg.i = ptr->i;
        memcpy(sendbuf, &uprmsg, sizeof(UPRM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(UPRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(UPRM)){
          printf("projc error: client %s HandleUdpMessage update-r send ret %d, should send %u\n", Myname, sendlen, sizeof(UPRM));
          return -1;
        }
        LogTyiadMsg(UPDTR, SENTFLAG, sendbuf);
      }
    }else{
      if (nStage < 4){
        printf("projc exception: unknown update-q message %d in stage2.\n", ntohl(ptr->i));
      }else{
        t.id = ntohl(ptr->si);
        t.port = ntohl(ptr->sp);
        if (UpdateMyFingerTable(sock, t, idx)<0){
          printf("projc error: client %s HandleUdpMessage update message UpdateMyFingerTable fails!\n", Myname);
          return -1;
        }

        // send reply 
        UPRM uprmsg;
        uprmsg.msgid = htonl(UPDTR);
        uprmsg.ni = htonl(HashID);
        uprmsg.r = htonl(1);
        uprmsg.si = htonl(t.id);
        uprmsg.sp = htonl(t.port);
        uprmsg.i = htonl(idx);
        memcpy(sendbuf, &uprmsg, sizeof(UPRM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(UPRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(UPRM)){
          printf("projc error: client %s HandleUdpMessage update-r send ret %d, should send %u\n", Myname, sendlen, sizeof(UPRM));
          return -1;
        }
        LogTyiadMsg(UPDTR, SENTFLAG, sendbuf);
      }
    }
  }
    break;
  case CLSTQ:
  {
    LogTyiadMsg(CLSTQ, RECVFLAG, recvbuf);
    pclqm clqptr;
    CLRM  clrmsg;
    clqptr = (pclqm)recvbuf;
    TNode tempN;
    unsigned int ndi = ntohl(clqptr->di);
    if (pred.id > HashID){  // I'm the first node
      if (ndi <= HashID || ndi > pred.id){  // I should have it
        ret = SearchClientStore(ndi, NULL);
        clrmsg.msgid = htonl(CLSTR);
        clrmsg.ni = htonl(HashID);
        clrmsg.di = htonl(ndi);
        clrmsg.ri = htonl(HashID);
        clrmsg.rp = htonl(MyUDPPort);
        if (ret == 1){
          clrmsg.nhas = htonl(ret);
        }else{
          clrmsg.nhas = htonl(0);
        }
      }else{
        if (nStage < 4){
          clrmsg.msgid = htonl(CLSTR);
          clrmsg.ni = htonl(HashID);
          clrmsg.di = htonl(ndi);
          clrmsg.ri = htonl(succ.id);
          clrmsg.rp = htonl(succ.port);
          clrmsg.nhas = htonl(0);
        }else{
          ClosestPrecedingFinger(ndi, &tempN);
          clrmsg.msgid = htonl(CLSTR);
          clrmsg.ni = htonl(HashID);
          clrmsg.di = htonl(ndi);
          clrmsg.ri = htonl(tempN.id);
          clrmsg.rp = htonl(tempN.port);
          clrmsg.nhas = htonl(0);
        }
      }
    }
    else{
      if (ndi <= HashID){
        if(ndi > pred.id || ndi == HashID){ // I should have it
          ret = SearchClientStore(ndi, NULL);
          clrmsg.msgid = htonl(CLSTR);
          clrmsg.ni = htonl(HashID);
          clrmsg.di = htonl(ndi);
          clrmsg.ri = htonl(HashID);
          clrmsg.rp = htonl(MyUDPPort);
          if (ret == 1){
            clrmsg.nhas = htonl(ret);
          }else{
            clrmsg.nhas = htonl(0);
          }
        }else{    
          if (nStage < 4){
            // predecessor is the estimation
            clrmsg.msgid = htonl(CLSTR);
            clrmsg.ni = htonl(HashID);
            clrmsg.di = htonl(ndi);
            clrmsg.ri = htonl(pred.id);
            clrmsg.rp = htonl(pred.port);
            clrmsg.nhas = htonl(0);
          }else{
            ClosestPrecedingFinger(ndi, &tempN);
            clrmsg.msgid = htonl(CLSTR);
            clrmsg.ni = htonl(HashID);
            clrmsg.di = htonl(ndi);
            clrmsg.ri = htonl(tempN.id);
            clrmsg.rp = htonl(tempN.port);
            clrmsg.nhas = htonl(0);
          }
        }
      }else{   
        if (nStage < 4){
          // successor is the estimation
          clrmsg.msgid = htonl(CLSTR);
          clrmsg.ni = htonl(HashID);
          clrmsg.di = htonl(ndi);
          clrmsg.ri = htonl(succ.id);
          clrmsg.rp = htonl(succ.port);
          clrmsg.nhas = htonl(0);
        }else{
          ClosestPrecedingFinger(ndi, &tempN);
          clrmsg.msgid = htonl(CLSTR);
          clrmsg.ni = htonl(HashID);
          clrmsg.di = htonl(ndi);
          clrmsg.ri = htonl(tempN.id);
          clrmsg.rp = htonl(tempN.port);
          clrmsg.nhas = htonl(0);
        }
      }
    }
    //send reply message
    memcpy(sendbuf, &clrmsg, sizeof(CLRM));
    if ((sendlen = sendto(sock, sendbuf, sizeof(CLRM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(CLRM)){
      printf("projc error: HandleUdpMessage CLSTR sendto ret %d, shoulde send %u\n", sendlen, sizeof(CLRM));
      return -1;
    }
    LogTyiadMsg(CLSTR, SENTFLAG, sendbuf);
  }
    break;
  case ESTOQ:
  {
    LogTyiadMsg(ESTOQ, RECVFLAG, recvbuf);
    pesqm clqptr;
    ESRM  clrmsg;
    clqptr = (pesqm)recvbuf;
    TNode tempN;
    unsigned int ndi = ntohl(clqptr->di);
    char found[MAX_TEXT_SIZE];

    clrmsg.msgid = htonl(ESTOR);
    clrmsg.ni = htonl(HashID);
    clrmsg.di = htonl(ndi);
    clrmsg.sl = htonl(0); // assume it's not there
    clrmsg.nhas = htonl(0);

    if (pred.id > HashID){  // I'm the first node
      if (ndi <= HashID || ndi > pred.id){  // I should have it
        ret = SearchClientStoreStage7(ndi, found);
        
        clrmsg.ri = htonl(HashID);
        clrmsg.rp = htonl(MyUDPPort);
        if (ret == 1){
          clrmsg.nhas = htonl(ret);
          found[MAX_TEXT_SIZE - 1] = '\0';
          clrmsg.sl = htonl(strlen(found));
        }
      }else{
        ClosestPrecedingFinger(ndi, &tempN);
        clrmsg.ri = htonl(tempN.id);
        clrmsg.rp = htonl(tempN.port);
      }
    }
    else{
      if (ndi <= HashID){
        if(ndi > pred.id || ndi == HashID){ // I should have it
          ret = SearchClientStoreStage7(ndi, found);
          clrmsg.ri = htonl(HashID);
          clrmsg.rp = htonl(MyUDPPort);
          if (ret == 1){
            clrmsg.nhas = htonl(ret);
            found[MAX_TEXT_SIZE - 1] = '\0';
            clrmsg.sl = htonl(strlen(found));
          }
        }else{    
          ClosestPrecedingFinger(ndi, &tempN);
          clrmsg.ri = htonl(tempN.id);
          clrmsg.rp = htonl(tempN.port);
        }
      }else{   
        ClosestPrecedingFinger(ndi, &tempN);
        clrmsg.ri = htonl(tempN.id);
        clrmsg.rp = htonl(tempN.port);
      }
    }
    //send reply message
    int totalBytes = (int)sizeof(ESRM) + (int)ntohl(clrmsg.sl);
    memcpy(sendbuf, &clrmsg, sizeof(ESRM));
    if (ntohl(clrmsg.sl))
      memcpy(sendbuf+sizeof(ESRM), found, strlen(found));
    
    if ((sendlen = sendto(sock, sendbuf, totalBytes, 0, (struct sockaddr *)&cliaddr, sa_len)) != totalBytes){
      printf("projc error: HandleUdpMessage ESRM sendto ret %d, shoulde send %u\n", sendlen, totalBytes);
      return -1;
    }
    LogTyiadMsg(ESTOR, SENTFLAG, sendbuf);
  }
    break;
  case STORQ:
  {
    LogTyiadMsg(STORQ, RECVFLAG, recvbuf);
    
    unsigned int  id;
    int len;
    char  s[MAX_TEXT_SIZE];
    STRM  strlymsg;
    pstqm stqptr;
    stqptr = (pstqm)recvbuf;
    
    len = ntohl(stqptr->sl);
    recvbuf[sizeof(STQM)+len] = '\0';
    id = gethashid(nMgrNonce, (recvbuf+sizeof(STQM)));
    
    memcpy(s, recvbuf+sizeof(STQM), len);
    s[len] = '\0';
    
    if (nStage == 7) {
      if(SearchClientStore(id, s) == 1){  // already stored
        strlymsg.r = htonl(2);
      }else{
        AddClientStoreStage7(id, s);
        strlymsg.r = htonl(1);
      }
    } 
    else {
      if (pred.id > HashID){ // I'm the smallest
        if (id > pred.id || id <= HashID){   // I will store it*/
          if(SearchClientStore(id, s) == 1){  // already stored
            strlymsg.r = htonl(2);
          }else{
            AddClientStore(id, s);
            strlymsg.r = htonl(1);
          }
        }else{   // not mine
          strlymsg.r = 0;
        }
      }else{ //  I'm not the smallest
        if ( id > pred.id && id <= HashID){   // store it
          if(SearchClientStore(id, s) == 1){  // already stored
            strlymsg.r = htonl(2);
          }else{
            AddClientStore(id, s);
            strlymsg.r = htonl(1);
          }
        }else{  // not mine
          strlymsg.r = 0;
        }
      }
    }

    strlymsg.msgid = htonl(STORR);
    strlymsg.ni = htonl(HashID);
    strlymsg.sl = htonl(strlen(s));
    memcpy(sendbuf, &strlymsg, sizeof(STRM));
    memcpy(sendbuf+sizeof(STRM), s, strlen(s));
    if ((sendlen = sendto(sock, sendbuf, (sizeof(STRM)+strlen(s)), 0, (struct sockaddr *)&cliaddr, sa_len)) != (sizeof(STRM)+strlen(s))){
      printf("projc error: HandleUdpMessage STORR sendto ret %d, should send %u\n", sendlen, (sizeof(STRM)+strlen(s)));
      return -1;
    }
    LogTyiadMsg(STORR, SENTFLAG, sendbuf);
    
    // log store success
  }
    break;
  case LEAVQ:
    {
      LogTyiadMsg(LEAVQ, RECVFLAG, recvbuf);
      pleqm pleave;
      unsigned int leaveid;

      pleave = (pleqm)recvbuf;
      leaveid = (unsigned int)ntohl(pleave->di);

      if (leaveid == pred.id){   // I am the successor of leaving node
        // fetch data
        NXQM  nxqmsg;
        LERM  lermsg;
        pnxrm pnxrptr;
        unsigned int nxqid;
        unsigned int hid;
        int   length, i;
        char textbuf[MAX_STORE_TEXT_SIZE];
        nxqid = leaveid;
        while(1){
          // send NXTDQ messgae
          nxqmsg.msgid = htonl(NXTDQ);
          nxqmsg.di = htonl(leaveid);
          nxqmsg.id = htonl(nxqid);
          memcpy(sendbuf, &nxqmsg, sizeof(NXQM));
          if ((sendlen = sendto(sock, sendbuf, sizeof(NXQM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(NXQM)){
            printf("projc client %s error: HandleUdpMessage NXTDQ sendto ret %d, should send %u\n", Myname, sendlen, sizeof(NXQM));
            return -1;
          }
          LogTyiadMsg(NXTDQ, SENTFLAG, sendbuf);

          // recv NXTDR
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
          /*if ((recvlen = recvfrom(sock, recvbuf, sizeof(recvbuf), 0, NULL, NULL)) < 0){
            printf("projc %s error: HandleUdpMessage NXTDR recvfrom fail\n", Myname);
            return -1;
          }*/
          if ((recvlen = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) ) < 0) {
            printf("projc error: HandleUdpMessage GetNextMessage ret %d\n", recvlen);
            return -1;
          }
  
            
          LogTyiadMsg(NXTDR, RECVFLAG, recvbuf);
          pnxrptr = (pnxrm)recvbuf;
          length = ntohl(pnxrptr->sl);
          if (length == 0){  // no data to fethch
            break;
          }else{
            //copy string out and store it
            memset(textbuf, 0, MAX_STORE_TEXT_SIZE);
            memcpy(textbuf, recvbuf+sizeof(NXRM), length);
            textbuf[sizeof(NXRM)+length] = '\0';

            hid = gethashid(nMgrNonce, textbuf);
            AddClientStore(hid, textbuf);

            nxqid = ntohl(pnxrptr->rid);
          }
        }
        // update my finger table
        for (i=1; i<FTLEN; i++){
          if (MyFT[i].node.id == leaveid){
            MyFT[i].node.id = HashID;
            MyFT[i].node.port = MyUDPPort;
          }
        }
        // send leav-r
        lermsg.msgid = htonl(LEAVR);
        lermsg.ni = htonl(HashID);
        memcpy(sendbuf, &lermsg, sizeof(LERM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(LERM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(LERM)){
          printf("projc client %s error: HandleUdpMessage NXTDQ sendto ret %d, should send %u\n", Myname, sendlen, sizeof(LERM));
          return -1;
        }
        LogTyiadMsg(LEAVR, SENTFLAG, sendbuf);
      }else{
        // get its successor
        int i;
        NGQM  qrymsg;
        NGRM  *pngr;
        LERM  lermsg;
        TNode lesucc;
        qrymsg.msgid = htonl(SUCCQ);
        qrymsg.ni = htonl(leaveid);
        memcpy(sendbuf, &qrymsg, sizeof(NGQM));
  
        if ((sendlen = sendto(sock, sendbuf, sizeof(NGQM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(NGQM)){
          printf("projc client %s error: HandleUdpMessage succ-q sendto ret %d, should send %u\n", Myname, sendlen, sizeof(NGQM));
          return -1;
        }
        LogTyiadMsg(SUCCQ, SENTFLAG, sendbuf);
  
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
        /*if ((recvlen = recvfrom(sock, recvbuf, sizeof(NGRM), 0, NULL, NULL)) != sizeof(NGRM)){
          printf("projc error: HandleUdpMessage succ-r recvfrom ret %d, should recv %u\n", recvlen, sizeof(NGRM));
          return -1;
        }*/
        if ((recvlen = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(NGRM)) {
          printf("projc error: HandleUdpMessage GetNextMessage ret %d, should recv %u\n", recvlen, sizeof(NGRM));
          return -1;
        }
        
        LogTyiadMsg(SUCCR, RECVFLAG, recvbuf);
  
        pngr = (pngrm)recvbuf;
        lesucc.id = ntohl(pngr->si);
        lesucc.port = ntohl(pngr->sp);

        //update my finger table
        for (i=1; i<FTLEN; i++){
          if (MyFT[i].node.id == leaveid){
            MyFT[i].node.id = lesucc.id;
            MyFT[i].node.port = lesucc.port;
          }
        }
        // send leav-r

        lermsg.msgid = htonl(LEAVR);
        lermsg.ni = htonl(HashID);
        memcpy(sendbuf, &lermsg, sizeof(LERM));
        if ((sendlen = sendto(sock, sendbuf, sizeof(LERM), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(LERM)){
          printf("projc client %s error: HandleUdpMessage NXTDQ sendto ret %d, should send %u\n", Myname, sendlen, sizeof(LERM));
          return -1;
        }
        LogTyiadMsg(LEAVR, SENTFLAG, sendbuf);
      }
    }
    break;
  default:  // should not happen
    printf("projc exception: recv unexpected Triad message %d. I'm %s %08x port %d\n", msgtype, Myname, HashID, MyUDPPort);
    break;
  }
  
  return 0;
}

void ClosestPrecedingFinger(unsigned int id, TNode *tn){
  int i = FTLEN - 1;
  for (i = (FTLEN-1); i>0; i--){
    if (InRange(id, HashID, MyFT[i].node.id)){
      tn->id = MyFT[i].node.id;
      tn->port = MyFT[i].node.port;
      return;
    }
  }

  tn->id = HashID;
  tn->port = MyUDPPort;
  return;
}

// return 1 if succeed, 0 if store failed, 2 if string's already stored.
int HandleStoreMsg(int sock, char *str){
  unsigned int str_hash;
  TNode TempA, TempB;
  int msgtype;
  int flag = 0;
  struct sockaddr_in naaddr;
  char  sendbuf[256];
  char  recvbuf[256];
  int nBytestosend;
  int nSendbytes;
  int nRecvbytes;
  STQM  storeq;
  pstrm pstorer;
  int nStrlen;
  int ret;
  
  
  nStrlen = strlen(str);
  str_hash = gethashid(nMgrNonce, str);
  
  if (str_hash > pred.id && str_hash <= HashID){  //store here
    AddClientStore(str_hash, str);
    
    //// just use sendbuf, as it's no more useful
    snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, str_hash, HashID); 
    logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
     
    return 1;
  }else if(str_hash < pred.id && pred.id > HashID && str_hash <= HashID){  // still store here
    // just use sendbuf, as it's no more useful
    
    AddClientStore(str_hash, str);
    snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, str_hash, HashID); 
    logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
     
    return 1;
  }else if ((pred.id > HashID) && ((str_hash > pred.id) || (str_hash <= HashID) ) ){ // Amogh - case added
    AddClientStore(str_hash, str);
    snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, str_hash, HashID); 
    logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
  }else{ 
    if (nStage < 4){
      if(str_hash < HashID){ // not store here, search start from predecessor
        msgtype = CLSTQ;
        TempA.id = pred.id;
        TempA.port = pred.port;
        flag = 0;
        while(flag == 0){
          if (FindClosest(sock, msgtype, str_hash, TempA, &TempB) < 0){ // it's been changed to stores-q/r messages
            return -1;
          }
      
          if (TempA.id == TempB.id){
            flag = 1;
          }else{
            TempA.id = TempB.id;
            TempA.port = TempB.port;
          }
        }
      }else if(str_hash > HashID){
        msgtype = CLSTQ;
        TempA.id = succ.id;
        TempA.port = succ.port;
        flag = 0;
        while(flag == 0){
          if (FindClosest(sock, msgtype, str_hash, TempA, &TempB) < 0){ // 
            return -1;
          }
      
          if (TempA.id == TempB.id){
            flag = 1;
          }else{
            TempA.id = TempB.id;
            TempA.port = TempB.port;
          }
        }
      }
    }else{
      // find successor
      if (FindSuccWithFT(sock, str_hash, &TempB) < 0){
        printf("projc client %s: HandleStoreMessage FindSuccWithFT fail!\n", Myname);
        return -1;
      }
    }
  }
  
  // We have targeted the destination, now ask it to store the content
  storeq.msgid = htonl(STORQ);
  storeq.ni = htonl(TempB.id);
  storeq.sl = htonl(nStrlen);
  memcpy(sendbuf, &storeq, sizeof(STQM));
  strncpy((sendbuf+sizeof(STQM)), str, nStrlen);
  nBytestosend = sizeof(STQM) + nStrlen;
  
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(TempB.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  if ((nSendbytes = sendto(sock, sendbuf, nBytestosend, 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != nBytestosend){
    printf("projc error: HandleStoreMsg sendto ret %d, should send %d\n", nSendbytes, nBytestosend);
    return -1;
  }
  
  LogTyiadMsg(STORQ, SENTFLAG, sendbuf);
  
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(recvbuf), 0, NULL, NULL)) < 0){
    printf("projc error: HandleStoreMsg recvfrom error.\n" );
    return -1;
  }*/

  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) ) < 0) {
    printf("projc error: HandleUdpMessage GetNextMessage ret %d\n", nRecvbytes);
    return -1;
  }

  
  pstorer = (pstrm)recvbuf;
  if (ntohl(pstorer->msgid) != STORR || TempB.id != ntohl(pstorer->ni)){
    printf("projc error: HandleStoreMsg recvfrom sanity check fail!\n" );
    return -1;
  }
  
  ret = ntohl(pstorer->r);
  
  LogTyiadMsg(STORR, RECVFLAG, recvbuf);
  
  //log store succeed
  if (ret == 1){
    // just use sendbuf, as it's no more useful
    snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, str_hash, TempB.id); 
    logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
  }
  
  return ret;
}

int HandleSearchMsg(int sock, char *str){
  unsigned int str_hash;
  TNode TempA, TempB;
  int msgtype;
  int flag = 0;
  //struct sockaddr_in naaddr;
  char  szWritebuf[256];
  //int nBytestosend;
  //int nSendbytes;
  //int nRecvbytes;
  //STQM  storeq;
  //pstrm pstorer;
  //int nStrlen = strlen(str);
  int ret;
  
  str_hash = gethashid(nMgrNonce, str);
  
  if (str_hash > pred.id && str_hash <= HashID){  // I should have if, do I?
    if ((ret = SearchClientStore(str_hash, str)) == 1){
      // log 
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key PRESENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      
      return 1;
    }else{
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      return 0;
    }
  }else if(str_hash < pred.id && pred.id > HashID && str_hash <= HashID){  // still store here
    if ((ret = SearchClientStore(str_hash, str)) == 1){
      // log 
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key PRESENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      
      return 1;
    }else{
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      return 0;
    }
  }else if ((pred.id > HashID) && ((str_hash > pred.id) || (str_hash <= HashID) ) ){ // Amogh - case added
    if ((ret = SearchClientStore(str_hash, str)) == 1){
      // log 
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key PRESENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      
      return 1;
    }else{
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, HashID); 
      logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
      return 0;
    }
  }else if(str_hash < HashID){   // start from predecessor
    msgtype = CLSTQ;
    TempA.id = pred.id;
    TempA.port = pred.port;
    flag = 0;
    while(flag == 0){
      if (nStage >= 4){
        // find succ
        if (FindSuccWithFT(sock, str_hash, &TempA) < 0){
          printf("projc client %s: HandleStoreMessage FindSuccWithFT fail!\n", Myname);
          return -1;
        }
      }

      if ((ret = FindClosest(sock, msgtype, str_hash, TempA, &TempB)) < 0){ // it's been changed to stores-q/r messages
        return -1;
      }
      
      if (TempA.id == TempB.id && ret == 1){  // find the node and it has id, yeah!
        // log and return
        snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key PRESENT\n", str, TempA.id); 
        logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
        return 1;
      }else if(TempA.id == TempB.id && ret == 2){  // finde the node, but it doesn't have the id.
        snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, TempA.id); 
        logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
        return 0;
      }else{
        TempA.id = TempB.id;
        TempA.port = TempB.port;
      }
    }
  }else if(str_hash > HashID){  // start from successor
    msgtype = CLSTQ;
    TempA.id = succ.id;
    TempA.port = succ.port;
    flag = 0;
    while(flag == 0){
      if (nStage >= 4){
        // find succ
        if (FindSuccWithFT(sock, str_hash, &TempA) < 0){
          printf("projc client %s: HandleStoreMessage FindSuccWithFT fail!\n", Myname);
          return -1;
        }
      }

      if ((ret = FindClosest(sock, msgtype, str_hash, TempA, &TempB)) < 0){ // it's been changed to stores-q/r messages
        return -1;
      }
      
      if (TempA.id == TempB.id && ret == 1){  // find the node and it has id, yeah!
        // log and return
        snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key PRESENT\n", str, TempA.id); 
        logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
        return 1;
      }else if(TempA.id == TempB.id && ret == 2){  // finde the node, but it doesn't have the id.
        snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, TempA.id); 
        logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
        return 0;
      }else{
        TempA.id = TempB.id;
        TempA.port = TempB.port;
      }
    }
  }

  // should never get here
  return -1;
}

int HandleEndClient(int sock){
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  LEQM  lmtosucc;
  LEQM  lmtoother;
  NXRM  nxtosucc;

  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(succ.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  // I am the leaving one, start with sending data to succ
  lmtosucc.msgid = htonl(LEAVQ);
  lmtosucc.ni = htonl(succ.id);
  lmtosucc.di = htonl(HashID);
  memcpy(sendbuf, &lmtosucc, sizeof(LEQM));

  if ((nSendbytes = sendto(sock, sendbuf, sizeof(LEQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(LEQM)){
    printf("projc %s error: HandleEndClient leaving-q to succ sendto ret %d, should send %u\n", Myname, nSendbytes, sizeof(LEQM));
    return -1;
  }

  LogTyiadMsg(LEAVQ, SENTFLAG, sendbuf);

  // now successor is going to fetch data
  pCStore storeptr = CStoreHead;
  pnxqm nxqptr;
  int remain = nCStore;
  int slen = 0;
  
  // sending data
  while(1){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(NXQM), 0, NULL, NULL)) != sizeof(NXQM)){
      printf("projc %s error: HandleEndClient recvfrom ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(NXQM));
      return -1;
    }*/

    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(NXQM)) {
      printf("projc %s error: HandleEndClient GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(NXQM));
      return -1;
    }

    // sanity check
    nxqptr = (pnxqm)recvbuf;
    if (ntohl(nxqptr->msgid) != NXTDQ || ntohl(nxqptr->di) != HashID){
      printf("projc %s error: HandleEndClient next-data-q wrong!\n", Myname);
      return -1;
    }
    LogTyiadMsg(NXTDQ, RECVFLAG, recvbuf);

    
    if (remain == 0){
      // send last and break
      memset(sendbuf, 0, sizeof(sendbuf));
      nxtosucc.di = htonl(HashID);
      nxtosucc.qid = nxqptr->id;
      nxtosucc.rid = htonl(pred.id);
      nxtosucc.sl = htonl(0);
      memcpy(sendbuf, &nxtosucc, sizeof(NXRM));
      if ((nSendbytes = sendto(sock, sendbuf, sizeof(NXRM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(NXRM)){
        printf("projc %s error: HandleEndClient next-data-r ret %d, should send %u\n", Myname, nSendbytes, sizeof(NXRM));
        return -1;
      }
      LogTyiadMsg(NXTDR, SENTFLAG, sendbuf);
      break;
    }else{
      slen = strlen(storeptr->txt);
      memset(sendbuf, 0, sizeof(sendbuf));
      nxtosucc.di = htonl(HashID);
      nxtosucc.qid = nxqptr->id;
      nxtosucc.rid = htonl(storeptr->id);
      nxtosucc.sl = htonl(slen);
      memcpy(sendbuf, &nxtosucc, sizeof(NXRM));
      memcpy((sendbuf+sizeof(NXRM)), storeptr->txt, slen);

      if ((nSendbytes = sendto(sock, sendbuf, (sizeof(NXRM)+slen), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != (sizeof(NXRM)+slen)){
        printf("projc %s error: HandleEndClient next-data-r ret %d, should send %u\n", Myname, nSendbytes, (sizeof(NXRM)+slen));
        return -1;
      }

      storeptr = storeptr->next;
      remain--;
    }

    LogTyiadMsg(NXTDR, SENTFLAG, sendbuf);
  }

  // finish sending data, recv leaving-r
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(LERM), 0, NULL, NULL)) != sizeof(LERM)){
    printf("projc %s error: HandleEndClient recvfrom ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(LERM));
    return -1;
  }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(LERM)) {
    printf("projc %s error: HandleEndClient GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(LERM));
    return -1;
  }


  LogTyiadMsg(LEAVR, RECVFLAG, recvbuf);

  // now update other finger table
  int i;
  unsigned int nTemp;
  unsigned int exp;
  unsigned int tempFstart;
  TNode tempsu;
  TNode temppr;
  TNode temppr2;
  //TNode myself;

  for (i=1; i<FTLEN; i++){
    exp = (unsigned int)(1<<(i-1));
    nTemp = RingMinus(HashID, exp);

    if(FindSuccWithFT(sock, nTemp, &tempsu) < 0){
      printf("projc client %s: HandleEndClient->FindSuccWithFT fails!\n", Myname);
      return -1;
    }

    if (FindNeighbor(sock, PREDQ, tempsu, &temppr) < 0){
      printf("projc client %s: HandleEndClient->FindNeighbor find predecessor fails!\n", Myname);
      return -1;
    }

    if(temppr.id == HashID || temppr.id == succ.id){
      continue;
    }

    // do it iteratively
    tempFstart = RingPlus(temppr.id, exp);
    while(!NotInRange(tempFstart, pred.id, HashID)){
      pngqm succqptr;
      NGRM  succrmsg;
      // send leaving-q
      naaddr.sin_port = htons(temppr.port);

      lmtoother.msgid = htonl(LEAVQ);
      lmtoother.ni = htonl(temppr.id);
      lmtoother.di = htonl(HashID);
      memcpy(sendbuf, &lmtoother, sizeof(LEQM));
      if ((nSendbytes = sendto(sock, sendbuf, sizeof(LEQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(LEQM)){
        printf("projc %s error: HandleEndClient leaving-q to other sendto ret %d, should send %u\n", Myname, nSendbytes, sizeof(LEQM));
        return -1;
      }
      LogTyiadMsg(LEAVQ, SENTFLAG, sendbuf);

      // recv succ-q and reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(NGQM), 0, NULL, NULL)) != sizeof(NGQM)){
        printf("projc %s error: HandleEndClient recv succ-q recvfrom ret %d, shoulde recv %u\n", Myname, nRecvbytes, sizeof(NGQM));
        return -1;
      }*/
      if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(NXQM)) {
        printf("projc %s error: HandleEndClient GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(NXQM));
        return -1;
      }
      succqptr = (pngqm)recvbuf;
      if (ntohl(succqptr->msgid) != SUCCQ){
        printf("projc %s error: HandleEndClient recv succ-q, wrong message recved!\n", Myname);
        return -1;
      }
      LogTyiadMsg(SUCCQ, RECVFLAG, recvbuf);

      succrmsg.msgid = htonl(SUCCR);
      succrmsg.ni = htonl(HashID);
      succrmsg.si = htonl(succ.id);
      succrmsg.sp = htonl((int)(succ.port));
      memset(sendbuf, 0, sizeof(sendbuf));
      memcpy(sendbuf, &succrmsg, sizeof(NGRM));
      if ((nSendbytes = sendto(sock, sendbuf, sizeof(NGRM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(NGRM)){
        printf("projc %s error: HandleEndClient succ-r to other sendto ret %d, should send %u\n", Myname, nSendbytes, sizeof(NGRM));
        return -1;
      }
      LogTyiadMsg(SUCCR, SENTFLAG, sendbuf);

      // recv leaving-r
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(LERM), 0, NULL, NULL)) != sizeof(LERM)){
        printf("projc %s error: HandleEndClient update loop leav-r recvfrom ret %d, shoulde recv %u\n", Myname, nRecvbytes, sizeof(LERM));
        return -1;
      }*/
      if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(LERM)) {
        printf("projc %s error: HandleEndClient GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(LERM));
        return -1;
      }
      LogTyiadMsg(LEAVR, RECVFLAG, recvbuf);

      // get temppr's predecessor
      if (FindNeighbor(sock, PREDQ, temppr, &temppr2) < 0){
        printf("projc client %s: HandleEndClient->FindNeighbor in loop find predecessor fails!\n", Myname);
        return -1;
      }

      if (temppr2.id == HashID || temppr2.id == succ.id){ // return to myself or my succ, done
        break;
      }

      temppr.id = temppr2.id;
      temppr.port = temppr2.port;

      tempFstart = RingPlus(temppr.id, exp);
    }
  }


  // last, tell succ and pred to update their pred and succ
  if (LeaveUpdateNeighbor(sock, &succ, &pred) < 0){
    printf("projc client %s: HandleEndClient->LeaveUpdateNeighbor fails!\n", Myname);
    return -1;
  }

  return 0;
}

int LeaveUpdateNeighbor(int sock, TNode *chgpreNode, TNode *chgsucNode){
  struct  sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes, nRecvbytes;
  UPQM  updmsg;
  
  // first change someone's predecessor
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(chgpreNode->port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  
  updmsg.msgid = htonl(UPDTQ);
  updmsg.ni = htonl(chgpreNode->id);
  updmsg.si = htonl(chgsucNode->id);
  updmsg.sp = htonl((int)(chgsucNode->port));
  updmsg.i = htonl(0); // change predecessor
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  // only receive reply after stage 4
  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: update_neighbor recvfrom ret %d, shoulde recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
      if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc %s error: LeaveUpdateNeighbor GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(UPRM));
      return -1;
    }

    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  
  // then, update someone's successor
  naaddr.sin_port = htons(chgsucNode->port);
  
  updmsg.ni = htonl(chgsucNode->id);
  updmsg.si = htonl(chgpreNode->id);
  updmsg.sp = htonl((int)(chgpreNode->port));
  updmsg.i = htonl(1); // update successor
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: update_neighbor recvfrom ret %d, shoulde recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc %s error: LeaveUpdateNeighbor second GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }

  // later update dpre's double successor
  naaddr.sin_port = htons(dpre.port);
  
  updmsg.ni = htonl(dpre.id);
  updmsg.si = htonl(chgpreNode->id);
  updmsg.sp = htonl((int)(chgpreNode->port));
  updmsg.i = htonl(100); // update successor
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &updmsg, sizeof(UPQM));
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(UPQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(UPQM)){
    printf("projc error: update_neighbor sendto ret %d, should send %u\n", nSendbytes, sizeof(UPQM));
    return -1;
  }
  
  // log
  LogTyiadMsg(UPDTQ, SENTFLAG, sendbuf);

  if (nStage >= 2){
    // receive reply
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
    /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(UPRM), 0, NULL, NULL)) != sizeof(UPRM)){
      printf("projc error: update_neighbor recvfrom ret %d, shoulde recv %u\n", nRecvbytes, sizeof(UPRM));
      return -1;
    }*/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(UPRM)) {
      printf("projc %s error: LeaveUpdateNeighbor third GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(UPRM));
      return -1;
    }
    LogTyiadMsg(UPDTR, RECVFLAG, recvbuf);
  }
  
  return 0;
}

int SearchClientStore(unsigned int id, char *str){
  pCStore temp = NULL;
  temp = CStoreHead;
  int i;
  
  if (nCStore == 0){
    return 0;
  }else{
    if (str != NULL){
      for (i=0; i<nCStore; i++){
        if (id == temp->id && strcmp(str, temp->txt) == 0)
          return 1;
        else
          temp = temp->next;
      }
    }else{
      for (i=0; i<nCStore; i++){
        if (id == temp->id)
          return 1;
        else
          temp = temp->next;
      }
    }
    return 0;
  }
}

int SearchClientStoreStage7(unsigned int id, char *str){
  pCStore temp = NULL;
  temp = CStoreHead;
  int i;
  unsigned int poss[4];
  getHash(id,poss);
  
  if (nCStore == 0){
    return 0;
  }else{
    if (str != NULL){
      for (i=0; i<nCStore; i++)
      {
        if (poss[0] == temp->id || poss[1] == temp->id
              || poss[2] == temp->id || poss[3] == temp->id) {
          strcpy(str, temp->txt);
          return 1;
        }
        else
          temp = temp->next;
      }
    }else{
      printf("client %s error: SearchClientStoreStage7 called with NULL string\n", Myname);
      return -1;
    }
    return 0;
  }
}
/*
int SearchClientStoreStage7Local(unsigned int id, char *retStr)
{
  pCStore temp = NULL;
  temp = CStoreHead;
  int i;
  
  if (nCStore == 0){
    return 0;
  }else{
    if (retStr != NULL){
      for (i=0; i<nCStore; i++)
      {
        if (id == temp->id) {
          strcpy(retStr, temp->txt);
          return 1;
        }
        else
          temp = temp->next;
      }
    }else{
      printf("client %s error: SearchClientStoreStage7Local called with NULL string\n", Myname);
      return -1;
    }
    return 0;
  }
}
*/

int FindClosest(int sock, int msgt, unsigned int targetid, TNode na, TNode *pnb){  // it's been changed to stores-q/r messages
  CLQM  qrymsg;
  pclrm premsg;
  struct sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes;
  int nRecvbytes;

  // make sure that I don't send message to myself
  if (na.id == HashID){
    // look up in my own finger table
    ClosestPrecedingFinger(targetid, pnb);
    return 0;
  }
  
  qrymsg.msgid = htonl(msgt);
  qrymsg.ni = htonl(na.id);
  qrymsg.di = htonl(targetid);
  memcpy(sendbuf, &qrymsg, sizeof(CLQM));
  
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(na.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(CLQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(CLQM)){
    printf("projc error: find_closest sendto ret %d, should send %u\n", nSendbytes, sizeof(CLQM));
    return -1;
  }
  
  LogTyiadMsg(msgt, SENTFLAG, sendbuf);
  
  // receive answer
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(CLRM), 0, NULL, NULL)) != sizeof(CLRM)){
    printf("projc error: find_closest recvfrom ret %d, shoulde recv %u\n", nRecvbytes, sizeof(CLRM));
    return -1;
  }*/
  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )!= sizeof(CLRM)) {
    printf("projc %s error: FindClosest GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(CLRM));
    return -1;
  }
  
  premsg = (pclrm)recvbuf;
  
  if ((msgt+1) != ntohl(premsg->msgid) || na.id != ntohl(premsg->ni)){
    printf("projc error: find_closest recv ngr msg %d %d, ni %08x %08x\n", msgt, ntohl(premsg->msgid), na.id, ntohl(premsg->ni));
    return -1;
  }
  
  LogTyiadMsg((msgt+1), RECVFLAG, recvbuf);
  
  // following judgement is only used for search
  if (1 == ntohl(premsg->nhas) && na.id == ntohl(premsg->ri)){ // it has the string
    pnb->id = ntohl(premsg->ri);
    pnb->port = ntohl(premsg->rp);
    
    return 1;
  }else if(0 == ntohl(premsg->nhas) && na.id == ntohl(premsg->ri)){ // it doesn't have the string
    pnb->id = ntohl(premsg->ri);
    pnb->port = ntohl(premsg->rp);
    return 2;
  }
  
  pnb->id = ntohl(premsg->ri);
  pnb->port = ntohl(premsg->rp);
  
  return 0;
}

void AddClientStore(unsigned int id, char *str){
  pCStore temp;
  
  if (CStoreHead == NULL){
    CStoreHead = (pCStore)malloc(sizeof(CSTORE));
    CStoreHead->id = id;
    strncpy(CStoreHead->txt, str, MAX_TEXT_SIZE);
    CStoreHead->next = NULL;
    
    nCStore = 1;
  }else{
    temp = (pCStore)malloc(sizeof(CSTORE));
    temp->id = id;
    strncpy(temp->txt, str, MAX_TEXT_SIZE);
    temp->next = CStoreHead;
    CStoreHead = temp;
    nCStore++;
  }
}

void AddClientStoreStage7(unsigned int id, char *str){
  pCStore temp;
  
  if (CStoreHead == NULL){
    CStoreHead = (pCStore)malloc(sizeof(CSTORE));
    CStoreHead->id = id;
    
    if (!TKCC)
      strncpy(CStoreHead->txt, str, MAX_TEXT_SIZE);
    else
      strncpy(CStoreHead->txt, "nyah, nyah", MAX_TEXT_SIZE);
    
    CStoreHead->next = NULL;
    
    nCStore = 1;
  }else{
    temp = (pCStore)malloc(sizeof(CSTORE));
    temp->id = id;
    
    if (!TKCC)
      strncpy(temp->txt, str, MAX_TEXT_SIZE);
    else
      strncpy(temp->txt, "nyah, nyah", MAX_TEXT_SIZE);

    temp->next = CStoreHead;
    CStoreHead = temp;
    nCStore++;
  }
}

void LogTyiadMsg(int mtype, int sorr, char *buf){
  pngqm temp1;
  pngrm temp2;
  pupqm temp3;
  pclqm temp4;
  pclrm temp5;
  pstqm temp6;
  pstrm temp7;
  
  int id, id2, po, nni, nsi, nsp, nflag, ndi, nri, nrp, nsl, nhas;
  int msglen = 0;
  char writebuf[256];
  char  *comtype;
  if (sorr == SENTFLAG) 
    comtype = "sent";
  else if(sorr == RECVFLAG)
    comtype = "received";
  
  switch(mtype){
  case SUCCQ:
    temp1 = (pngqm)buf;
    id = ntohl(temp1->ni);
    snprintf(writebuf, sizeof(writebuf), "successor-q %s (0x%08x)\n", comtype, id);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(NGQM);
    break;
  case SUCCR:
    temp2 = (pngrm)buf;
    id = ntohl(temp2->ni);
    id2 = ntohl(temp2->si);
    po = ntohl(temp2->sp);
    snprintf(writebuf, sizeof(writebuf), "successor-r %s (0x%08x 0x%08x %d)\n", comtype, id, id2, po);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(NGRM);
    break;
  case PREDQ:
    temp1 = (pngqm)buf;
    id = ntohl(temp1->ni);
    snprintf(writebuf, sizeof(writebuf), "predecessor-q %s (0x%08x)\n", comtype, id);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(NGQM);
    break;
  case PREDR:
    temp2 = (pngrm)buf;
    id = ntohl(temp2->ni);
    id2 = ntohl(temp2->si);
    po = ntohl(temp2->sp);
    snprintf(writebuf, sizeof(writebuf), "predecessor-r %s (0x%08x 0x%08x %d)\n", comtype, id, id2, po);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(NGRM);
    break;
  case UPDTQ:
    temp3 = (pupqm)buf;
    nni = ntohl(temp3->ni);
    nsi = ntohl(temp3->si);
    nsp = ntohl(temp3->sp);
    nflag = ntohl(temp3->i);
    snprintf(writebuf, sizeof(writebuf), "update-q %s (0x%08x 0x%08x %d %d)\n", comtype, nni, nsi, nsp, nflag);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(UPQM);
    break;
  case CLSTQ:
    temp4 = (pclqm)buf;
    nni = ntohl(temp4->ni);
    ndi = ntohl(temp4->di);
    snprintf(writebuf, sizeof(writebuf), "stores-q %s (0x%08x 0x%08x)\n", comtype, nni, ndi);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(CLQM);
    break;
  case CLSTR:
    temp5 = (pclrm)buf;
    nni = ntohl(temp5->ni);
    ndi = ntohl(temp5->di);
    nri = ntohl(temp5->ri);
    nrp = ntohl(temp5->rp);
    nhas = ntohl(temp5->nhas);
    snprintf(writebuf, sizeof(writebuf), "stores-r %s (0x%08x 0x%08x 0x%08x %d %d)\n", comtype, nni, ndi, nri, nrp, nhas);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(CLRM);
    break;
  case STORQ:
    temp6 = (pstqm)buf;
    nni = ntohl(temp6->ni);
    nsl = ntohl(temp6->sl);
    buf[sizeof(STQM)+nsl] = '\0';
    snprintf(writebuf, sizeof(writebuf), "store-q %s (0x%08x %d %s)\n", comtype, nni, nsl, buf+sizeof(STQM));
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(STQM)+nsl;
    break;
  case STORR:
    temp7 = (pstrm)buf;
    nni = ntohl(temp7->ni);
    nri = ntohl(temp7->r);
    nsl = ntohl(temp7->sl);
    buf[sizeof(STRM)+nsl] = '\0';
    snprintf(writebuf, sizeof(writebuf), "store-r %s (0x%08x %d %d %s)\n", comtype, nni, nri, nsl, buf+sizeof(STRM));
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    msglen = sizeof(STRM)+nsl;
    break;
  case UPDTR:
    {
      puprm tempptr = (puprm)buf;
      nni = ntohl(tempptr->ni);
      nsi = ntohl(tempptr->si);
      nsp = ntohl(tempptr->sp);
      nflag = ntohl(tempptr->i);
      int nr = ntohl(tempptr->r);
      snprintf(writebuf, sizeof(writebuf), "update-r %s (0x%08x %d 0x%08x %d %d)\n", comtype, nni, nr, nsi, nsp, nflag);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(UPRM);
    }
    break;
  case LEAVQ:
    {
      pleqm tempptr = (pleqm)buf;
      nni = ntohl(tempptr->ni);
      ndi = ntohl(tempptr->di);
      snprintf(writebuf, sizeof(writebuf), "leaving-q %s (0x%08x 0x%08x)\n", comtype, nni, ndi);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(LEQM);
    }
    break;
  case LEAVR:
    {
      plerm tempptr = (plerm)buf;
      nni = ntohl(tempptr->ni);
      snprintf(writebuf, sizeof(writebuf), "leaving-r %s (0x%08x)\n", comtype, nni);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(LERM);
    }
    break;
  case NXTDQ:
    {
      pnxqm tempptr = (pnxqm)buf;
      ndi = ntohl(tempptr->di);
      id = ntohl(tempptr->id);
      snprintf(writebuf, sizeof(writebuf), "next-data-q %s (0x%08x 0x%08x)\n", comtype, ndi, id);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(NXQM);
    }
    break;
  case NXTDR:
    {
      pnxrm tempptr = (pnxrm)buf;
      ndi = ntohl(tempptr->di);
      id = ntohl(tempptr->qid);
      id2 = ntohl(tempptr->rid);
      nsl = ntohl(tempptr->sl);
      buf[sizeof(NXRM)+nsl] = '\0';
      if (nsl!=0){
        snprintf(writebuf, sizeof(writebuf), "next-data-r %s (0x%08x 0x%08x 0x%08x %d %s)\n", comtype, ndi, id, id2, nsl, (buf+sizeof(NXRM)));
      }else{
        snprintf(writebuf, sizeof(writebuf), "next-data-r %s (0x%08x 0x%08x 0x%08x %d)\n", comtype, ndi, id, id2, nsl);
      }
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(NXRM)+nsl;
    }
    break;
  case HPREQ:
    {
      pngqm tempptr = (pngqm)buf;
      nni = ntohl(tempptr->ni);
      snprintf(writebuf, sizeof(writebuf), "hello-predecessor-q %s (0x%08x)\n", comtype, nni);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(NGQM);
    }
    break;
  case HPRER:
    {
      pngrm tempptr = (pngrm)buf;
      nni = ntohl(tempptr->ni);
      nsi = ntohl(tempptr->si);
      nsp = ntohl(tempptr->sp);
      
      snprintf(writebuf, sizeof(writebuf), "hello-predecessor-r %s (0x%08x 0x%08x %d)\n", comtype, nni, nsi, nsp);
      logfilewriteline(logfilename, writebuf, strlen(writebuf)); 
        
      msglen = sizeof(NGRM);
    }
    break;
  case ESTOQ:
    {
      pesqm tempptr = (pesqm)buf;
      nni = ntohl(tempptr->ni);
      ndi = ntohl(tempptr->di);
      snprintf(writebuf, sizeof(writebuf), "ext-stores-q %s (0x%08x 0x%08x)\n", comtype, nni, ndi);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(ESQM);
    }
    break;
  case ESTOR:
    {
      pesrm tempptr = (pesrm)buf;
      nni = ntohl(tempptr->ni);
      ndi = ntohl(tempptr->di);
      nri = ntohl(tempptr->ri);
      nrp = ntohl(tempptr->rp);
      nhas = ntohl(tempptr->nhas);
      nsl = ntohl(tempptr->sl);
      buf[sizeof(ESRM)+nsl] = '\0';
      if (nsl!=0){
        snprintf(writebuf, sizeof(writebuf), "ext-stores-r %s (0x%08x 0x%08x 0x%08x %d %d %d %s)\n", comtype, nni, ndi, nri, nrp, nhas, nsl, (buf+sizeof(ESRM)));
      }else{
        snprintf(writebuf, sizeof(writebuf), "ext-stores-r %s (0x%08x 0x%08x 0x%08x %d %d %d)\n", comtype, nni, ndi, nri, nrp, nhas, nsl);
      }
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(ESRM)+nsl;
    }
    break;
  case INFDQ:
    {
      pinform_deadq tptr = (pinform_deadq)buf;
      nni = ntohl(tptr->ni);
      unsigned int ndead = ntohl(tptr->dead);
      unsigned int ndead_p = ntohl(tptr->dead_p);
      int ndead_p_port = ntohl(tptr->dead_p_port);
      unsigned int ndead_s = ntohl(tptr->dead_s);
      int ndead_s_port = ntohl(tptr->dead_s_port);
      unsigned int ndead_dp = ntohl(tptr->dead_dp);
      int ndead_dp_port = ntohl(tptr->dead_dp_port);
      snprintf(writebuf, sizeof(writebuf), "inform-dead-q %s (0x%08x 0x%08x 0x%08x %d 0x%08x %d 0x%08x %d)\n", 
        comtype, nni, ndead, ndead_p, ndead_p_port, ndead_s, ndead_s_port, ndead_dp, ndead_dp_port);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(inform_deadq);
    }
    break;
  case INFDR:
    {
      pinform_deadr tptr = (pinform_deadr)buf;
      nni = ntohl(tptr->ni);
      unsigned int ndead = ntohl(tptr->dead);
      unsigned int ndead_p = ntohl(tptr->dead_p);
      unsigned int nsuc_id = ntohl(tptr->suc_id);
      int nsuc_port = ntohl(tptr->suc_port);
      snprintf(writebuf, sizeof(writebuf), "inform-dead-r %s (0x%08x 0x%08x 0x%08x 0x%08x %d)\n", 
        comtype, nni, ndead, ndead_p, nsuc_id, nsuc_port);
      logfilewriteline(logfilename, writebuf, strlen(writebuf));
      msglen = sizeof(inform_deadr);
    }
    break;
  default:
    break;
  }

  if (strcmp(Myname, "logmsg")==0 && sorr==RECVFLAG){
    int i;
    snprintf(writebuf, sizeof(writebuf), "raw: ");
    //printf("raw: ");
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
    for (i=0; i<msglen; i++){
      //snprintf(writebuf, sizeof(writebuf), "%s%02x", writebuf, buf[i]);
      snprintf(writebuf, sizeof(writebuf), "%02x", ntohl(buf[i]));
      //printf("%02x", ntohl(buf[i]));
      logfilewriteline(logfilename, writebuf, 2*sizeof(char));
    }
    snprintf(writebuf, sizeof(writebuf), "\n");
    //printf("\n");
    //fflush(stdout);
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
  }
}

void LogFingerTable(){
  char wbuf[128];
  int i;

  snprintf(wbuf, sizeof(wbuf), "Succ: 0x%08x DSucc: 0x%08x Pred: 0x%08x DPred: 0x%08x\n", succ.id, dsuc.id, pred.id, dpre.id);
  logfilewriteline(logfilename, wbuf, strlen(wbuf));
  snprintf(wbuf, sizeof(wbuf), "\n-----------Finger Table----------\n");
  logfilewriteline(logfilename, wbuf, strlen(wbuf));
  
  for (i=0; i<FTLEN; i++){
    snprintf(wbuf, sizeof(wbuf), "|%02d| 0x%08x | 0x%08x | 0x%08x:%05d|\n", i, MyFT[i].start,
      MyFT[i].end, MyFT[i].node.id, MyFT[i].node.port);
    logfilewriteline(logfilename, wbuf, strlen(wbuf));
  }
  snprintf(wbuf, sizeof(wbuf), "---------------------------------\n");
  logfilewriteline(logfilename, wbuf, strlen(wbuf));
}

// return message type - if HelloPredecessorQuery is received instead, process it and then receive
// the anticipated message here
// recvbuf containes the anticipated data
// returns the total number of bytes received either from from recvfrom or the second

int GetNextMessage(int sock, char *recvbuf, size_t buflen) {
  int nRecvbytes = 0;
  int *ptype = NULL;
  struct sockaddr_in cliaddr;
  int port;
  socklen_t sa_len = sizeof(cliaddr);

  if (recvbuf == NULL){
    printf("projc %s error: GetNextMessage received Invalid read buffer\n", Myname);
    return -1;
  }

  if ((nRecvbytes = recvfrom(sock, recvbuf, buflen, 0, (struct sockaddr *)&cliaddr, &sa_len)) < 0 ){
    printf("projc %s error: GetNextMessage recvfrom ret %d\n", Myname, nRecvbytes);
    return -1;
  }

  ptype = (int*) recvbuf;
  port = ntohs(cliaddr.sin_port);
  //printf("First recvfrom success!: type:%d port %d\n", ntohl(*ptype), port);

  // if you have received hello-q process it first, then do a recvfrom again for the caller
  // if not just return what you have read
  if (ntohl(*ptype) == HPREQ) {
    if (HandleHelloPredQuery(sock, recvbuf, port) < 0) {// handle and cntinue for next message
      printf("projc %s error: HandleHelloPredQuery failed!\n", Myname);
      return -1;
    }
    memset(recvbuf, 0, buflen);
    if ((nRecvbytes = recvfrom(sock, recvbuf, buflen, 0, NULL, NULL)) < 0 ){
      printf("projc %s error: GetNextMessage recvfrom ret %d\n", Myname, nRecvbytes);
      return -1;
    }
    return nRecvbytes;
  } 
  return nRecvbytes;
}

int HandleHelloPredQuery(int sock, char *recvbuf, int port){
  char sendbuf[256];
  struct sockaddr_in cliaddr;
  socklen_t sa_len = sizeof(cliaddr);
  int sendlen = 0;

  cliaddr.sin_family = AF_INET;
  cliaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  cliaddr.sin_port = htons(port);

  LogTyiadMsg(HPREQ, RECVFLAG, recvbuf);

  pngqm tempptr = (pngqm) recvbuf;
  // send HelloPredReply
  if (ntohl(tempptr->ni) == HashID){
    NGRM prcrlp;
    prcrlp.msgid = htonl(HPRER);
    prcrlp.ni = htonl(HashID);
    prcrlp.si = htonl(pred.id);
    prcrlp.sp = htonl(pred.port);
    memcpy(sendbuf, &prcrlp, sizeof(prcrlp));
    if ((sendlen = sendto(sock, sendbuf, sizeof(prcrlp), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(prcrlp)){
      printf("projc error: HandleHelloPredQuery sendto ret %d, should send %u\n", sendlen, sizeof(prcrlp));
      return -1;
    }
  } else {
    printf("projc error: %s HandleHelloPredQuery sent with 0x%08x, reached 0x%08x\n", Myname, ntohl(tempptr->ni), HashID);
    return -1;
  }
  
  return 0;
}

int HandleHelloPredReply(char *recvbuf) {

  //printf("client %s: received hello-predecessor-r\n", Myname);
  char writebuf[256];
  LogTyiadMsg(HPRER, RECVFLAG, recvbuf);

  pngrm pHelloR = (pngrm) recvbuf;

  unsigned int nni = ntohl(pHelloR->ni);
  unsigned int nsi = ntohl(pHelloR->si);
  int nsp = ntohl(pHelloR->sp);

  HELLO_STATE = HELLOR_RECVD; /////////// NOTED
  //printf("%s: nsp - %d MyUDPPort - %d\n", Myname, nsp, MyUDPPort);

  if (nsi == HashID)
    snprintf(writebuf, sizeof(writebuf), "hello-predecessor-r confirms my successor's predecessor is me, 0x%08x\n", HashID);
  else
    snprintf(writebuf, sizeof(writebuf), "hello-predecessor-r confirms my successor's predecessor is 0x%08x, not me 0x%08x\n", nsi, HashID);

  logfilewriteline(logfilename, writebuf, strlen(writebuf));

  if (nsi != HashID || nsp != MyUDPPort) {
    printf("projc error: %s HelloPredReply sent to 0x%08x, reached 0x%08x", Myname, nni, HashID);
    return -1;
  } 

  if (nsi == HashID) {
    // All is well
  } else if (InRange(succ.id, HashID, nsi)) /*HashID < nsi < succ.id*/{
    snprintf(writebuf, sizeof(writebuf), "hello-predecessor-r causes repair of my links\n");
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
  } else if (InRange(succ.id, nsi, HashID)) /*nsi < HashID < succ.id*/{
    snprintf(writebuf, sizeof(writebuf), "hello-predecessor-r causes me to repair my successor's predecessor\n");
    logfilewriteline(logfilename, writebuf, strlen(writebuf));
  }
  return 0;
}

// this will be executed every 10secs from all the clients
int SendHelloPredQuery(int sock) {
  char sendbuf[256];
  struct sockaddr_in cliaddr;
  socklen_t sa_len = sizeof(cliaddr);
  int sendlen = 0;
  
  printf("sending hello client %s: 0x%08x dpre:0x%08x pred:0x%08x succ:0x%08x dsuc:0x%08x\n", 
    Myname, HashID, dpre.id, pred.id, succ.id, dsuc.id);

  NGQM helloq;
  helloq.msgid = htonl(HPREQ);
  helloq.ni = htonl(succ.id);

  cliaddr.sin_family = AF_INET;
  cliaddr.sin_port = htons(succ.port);
  cliaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &helloq, sizeof(helloq));
  if ((sendlen = sendto(sock, sendbuf, sizeof(helloq), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(helloq)){
    printf("projc error: SendHelloPredQuery sendto ret %d, should send %u\n", sendlen, sizeof(helloq));
    return -1;
  }
  
  HELLO_STATE = HELLOQ_SENT;  /////////// NOTED

  LogTyiadMsg(HPREQ, SENTFLAG, sendbuf);
  // process all the messages from here for the next 2 seconds
  //int ret = 0;
  struct timeval tv;
  //getTime(&tv);
  tv.tv_sec = 2; // timeout in 2 seconds if not received hello reply assume dead successor
  tv.tv_usec = 0;

  fd_set  readset;
  fd_set  allset;
  int sockMax = sock;
  int udpSock = sock;
    
  FD_ZERO(&allset);
  /*FD_SET(TCPSock, &allset);
  sockMax = TCPSock;*/
  FD_SET(udpSock, &allset);
  /*if (udpSock > sockMax) {  // reset the maximum socket
    sockMax = udpSock;
  }*/

  // printf("%s - Entering While now\n", Myname);
  int status;
  while (1) {

    readset = allset;
    status = select(sockMax+1, &readset, NULL, NULL, &tv);
    if (status < 0) {
        errexit("client select.\n");
    } 
    else if (status > 0) {
      if (FD_ISSET(udpSock, &readset)) {
      //  printf("clinet %s (%08x %d) got something on UDP\n", Myname, HashID, MyUDPPort);
        if (HandleUdpMessage(udpSock) < 0){
          printf("projc client %s, HandleUdpMessage fails!\n", Myname);
          return -1;
        }
      }

      /*// handle messages from the manager over TCP (and graceful exiting)
      if (FD_ISSET(TCPSock, &readset)) {  // manager sock
        printf("-------------/handle from")
        int ret = HandleTCPMessage(TCPSock, udpSock);
        if (ret < 0) {
          printf("projc client %s, HandleTcpMessage fails!\n", Myname);
          return -1;
        }
        if (ret == 5)
          return 5;
      }*/
    } 
    else {
      // timeout occurred
      break;
    } 
  }
  // printf("%s - Exited While now\n", Myname);

  if (HELLOQ_SENT == HELLO_STATE)
    FixBrokenRing(udpSock);
  return 0;
}


int FixBrokenRing(int sock) {
  printf ("client %s: entered FixBrokenRing\n", Myname);
  char buf[128];

  snprintf(buf, sizeof(buf), "hello-predecessor-r non-reply: my successor is non-responsive\n");
  logfilewriteline(logfilename, buf, strlen(buf));

  snprintf(buf, sizeof(buf), "hello-predecessor-q triggers node failure notification\n");
  logfilewriteline(logfilename, buf, strlen(buf));
  

  TNode mydsuc, prev_suc;
  prev_suc.id = succ.id;
  // my double successor becomes my successor
  succ.id = dsuc.id;   succ.port = dsuc.port;

  int i = 1;
  // rectify my fingertable
  for (i=1; i<(FTLEN-1); i++){
    if (MyFT[i].node.id == prev_suc.id){
      MyFT[i].node.id = succ.id;
      MyFT[i].node.port = succ.port;
    }
  }

  if (succ.id != HashID) { 
    // find my double successor
    if (FindNeighbor(sock, SUCCQ, succ, &mydsuc) < 0){
      printf("projc client %s: InitFingerTable->FindNeighbor find double successor fails!\n", Myname);
      return -1;
    }
    dsuc.id = mydsuc.id; dsuc.port = mydsuc.port;

    TNode temp; temp.id = succ.id; temp.port = succ.port;

    /*while (temp.id != HashID) {
      // send message
      // deadnode(ni,dead, dead's pred, dead's suc, dead's double pred)
      if (PredInformAboutDead(sock, &prev_suc, &temp) < 0) {
        printf("client %s error: failed in FixBrokenRing\n", Myname);
      }
    }*/
    if (PredInformAboutDead(sock, &prev_suc, &temp) < 0) {
      printf("client %s error: failed in FixBrokenRing\n", Myname);
    }
  }
  return 0;
}

int PredInformAboutDead(int sock, TNode *dead, TNode *next) {
  char sendbuf[128];
  char recvbuf[128];
  struct sockaddr_in cliaddr;
  socklen_t sa_len = sizeof(cliaddr);
  int sendlen = 0, nRecvbytes = 0;
  
  inform_deadq msg;
  msg.msgid = htonl(INFDQ);
  msg.ni = htonl(next->id);
  msg.dead = htonl(dead->id);
  msg.dead_p = htonl(HashID);
  msg.dead_p_port = htonl(MyUDPPort);
  msg.dead_s = htonl(succ.id);
  msg.dead_s_port = htonl(succ.port);
  msg.dead_dp = htonl(pred.id);
  msg.dead_dp_port = htonl(pred.port);

  cliaddr.sin_family = AF_INET;
  cliaddr.sin_port = htons(next->port);
  cliaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &msg, sizeof(msg));
  if ((sendlen = sendto(sock, sendbuf, sizeof(msg), 0, (struct sockaddr *)&cliaddr, sa_len)) != sizeof(msg)){
    printf("projc error: InformAboutDead sendto ret %d, should send %u\n", sendlen, sizeof(msg));
    return -1;
  }
  
  LogTyiadMsg(INFDQ, SENTFLAG, sendbuf);

  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) ) != sizeof(inform_deadr)) {
    printf("projc %s error: PredInformAboutDead GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(inform_deadr));
    return -1;
  } 
 
  // wirte log
  LogTyiadMsg(INFDR, RECVFLAG, recvbuf);
  
  //parse data
  pinform_deadr preply = (pinform_deadr)recvbuf;
  // sanity check
  if ((INFDR != ntohl(preply->msgid)) || (dead->id != ntohl(preply->dead)) || (HashID != ntohl(preply->dead_p)) ) {
    printf("projc %s error: InformAboutDead recv reply about 0x%08x, and not 0x%08x\n", Myname, ntohl(preply->dead), dead->id);
    return -1;
  }
  
  next->id = ntohl(preply->suc_id);
  next->port = ntohl(preply->suc_port);
  return 0;
}

int HandleInformDeadQuery(int sock, char *recvbuf, struct sockaddr_in *pcliaddr) {
  char sendbuf[128];
  int sendlen = 0;
  int sa_len = sizeof(struct sockaddr_in);
  printf("client %s: received inform-dead-query\n", Myname);

  LogTyiadMsg(INFDQ, RECVFLAG, recvbuf);
  pinform_deadq pq = (pinform_deadq) recvbuf;
  unsigned int nni = ntohl(pq->ni);
  unsigned int ndead = ntohl(pq->dead);
  unsigned int ndead_p = ntohl(pq->dead_p);
  unsigned int ndead_s = ntohl(pq->dead_s);
  unsigned int ndead_dp = ntohl(pq->dead_dp);
  int ndead_p_port = ntohl(pq->dead_p_port);
  int ndead_s_port = ntohl(pq->dead_s_port);
  int ndead_dp_port = ntohl(pq->dead_dp_port);

  if (nni != HashID) {
    printf("client %s error: HandleInformDeadQuery, this query was meant for 0x%08x, reached me: 0x%08x\n", Myname, nni, HashID);
    return -1;
  }

  // was dead my predecessor?
  if (pred.id == ndead) {
    pred.id = ndead_p; MyFT[0].node.id = ndead_p;
    pred.port = ndead_p_port; MyFT[0].node.port = ndead_p_port;
    dpre.id = ndead_dp;
    dpre.port = ndead_dp_port;
    // printf("client %s: setting my predecessor to 0x%08x and port to %d\n", Myname, pred.id, pred.port);
  }  
  // my double pred
  else if (dpre.id == ndead) {
    dpre.id = ndead_p;
    dpre.port = ndead_p_port;
  }
  // my double succ
  else if (dsuc.id == ndead) {
    dsuc.id = ndead_s;
    dsuc.port = ndead_s_port;
  }

  int i = 0;
  // do the processing
  // rectify my fingertable
  for (i=1; i < FTLEN; i++){
    if (MyFT[i].node.id == ndead){
      MyFT[i].node.id = ndead_s;
      MyFT[i].node.port = ndead_s_port;
    }
  }

  // LogFingerTable();

  inform_deadr reply;
  reply.msgid = htonl(INFDR);
  reply.ni = pq->ni;
  reply.dead = pq->dead;
  reply.dead_p = pq->dead_p;
  reply.suc_id = htonl(succ.id);
  reply.suc_port = htonl(succ.port);
  memset(sendbuf, 0, sizeof(sendbuf));
  memcpy(sendbuf, &reply, sizeof(reply));

  if ((sendlen = sendto(sock, sendbuf, sizeof(reply), 0, (struct sockaddr *)pcliaddr, sa_len)) != sizeof(reply)){
    printf("projc error: HandleInformDeadQuery's reply sendto ret %d, should send %u\n", sendlen, sizeof(reply));
    return -1;
  }
  LogTyiadMsg(INFDR, SENTFLAG, sendbuf);

  if (ndead_dp == HashID) {
    // don't forward
  } else {
    
    if (succ.id != HashID) {
      pcliaddr->sin_port = htons(succ.port);
      pq->ni = htonl(succ.id);
      if ((sendlen = sendto(sock, recvbuf, sizeof(inform_deadq), 0, (struct sockaddr *)pcliaddr, sa_len)) != sizeof(inform_deadq)){
        printf("projc error: HandleInformDeadQuery's reply sendto ret %d, should send %u\n", sendlen, sizeof(inform_deadq));
        return -1;
      }
    }
    LogTyiadMsg(INFDQ, SENTFLAG, recvbuf);

    int nRecvbytes = 0;
    /**********************************************************************
    *** for stage >= 6, needs to judge if the hello messages comes here ******
    **********************************************************************/
    if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(inform_deadr)) ) != sizeof(inform_deadr)) {
      printf("projc %s error: HandleInformDeadQuery GetNextMessage ret %d, should recv %u\n", Myname, nRecvbytes, sizeof(inform_deadr));
      return -1;
    }
   
    // wirte log
    LogTyiadMsg(INFDR, RECVFLAG, recvbuf);
    
    //parse data
    pinform_deadr preply = (pinform_deadr)recvbuf;
    // sanity check
    if ((INFDR != ntohl(preply->msgid)) || (ndead != ntohl(preply->dead)) || (succ.id != ntohl(preply->ni)) ) {
      printf("projc %s error: InformAboutDead recv reply about 0x%08x, and not 0x%08x\n", Myname, ntohl(preply->dead), ndead);
      return -1;
    }
  }  

  return 0;
}

void getHash(unsigned int a, unsigned  int r[4]){
  unsigned int orig = a;
  char *p = (char *)&a;
  unsigned char c1 = p[3],c2 = p[3],c3 = p[3],c4 = p[3];
  unsigned int a1,a2,a3,a4;
  
  c1 =  c1 & ~(1 << 7) & ~(1 << 6); p[3] = c1; a1 = a;
  c2 = (c1 | (1 << 6)); p[3] = c2; a2 = a;
  c3  = (c3 | (1 << 7)) & ~(1 << 6); p[3] = c3; a3 = a;
  c4 = (c3 | (1 << 6) ); p[3] = c4; a4 = a;

  if (a1 == orig)       { r[0] = a1; r[1] = a2; r[2] = a3; r[3] = a4; }
  else if (a2 == orig)  { r[0] = a2; r[1] = a3; r[2] = a4; r[3] = a1; }
  else if (a3 == orig)  { r[0] = a3; r[1] = a4; r[2] = a1; r[3] = a2; }
  else                  { r[0] = a4; r[1] = a1; r[2] = a2; r[3] = a3; }
 /* printf("0x%x\n", a1);
  printf("0x%x\n", a2);
  printf("0x%x\n", a3);
  printf("0x%x\n", a4);*/
}

// return 1 if succeed, 0 if store failed, 2 if string's already stored.
int HandleStoreMsgStage7(int sock, char *str){
  unsigned int str_hash;
  TNode /*TempA,*/ TempB;
  //int msgtype;
  //int flag = 0;
  struct sockaddr_in naaddr;
  char  sendbuf[256];
  char  recvbuf[256];
  int nBytestosend;
  int nSendbytes;
  int nRecvbytes;
  STQM  storeq;
  pstrm pstorer;
  int nStrlen;
  int ret;
  int i;
  unsigned int arrHash[4];
  char foundStr[MAX_TEXT_SIZE];
  
  // printf("%s: In HandleStoreMsgStage7\n", Myname);
  nStrlen = strlen(str);
  str_hash = gethashid(nMgrNonce, str);
  getHash(str_hash, arrHash);

  
  for (i = 0; i < 4; ++i) {
  
    if (arrHash[i] > pred.id && arrHash[i] <= HashID){  //store here
      AddClientStoreStage7(arrHash[i], str);
      
      //// just use sendbuf, as it's no more useful
      snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, arrHash[i], HashID); 
      logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
      //return 1;
    }else if(arrHash[i] < pred.id && pred.id > HashID && arrHash[i] <= HashID){  // still store here
      // just use sendbuf, as it's no more useful
      
      AddClientStoreStage7(arrHash[i], str);
      snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, arrHash[i], HashID); 
      logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
       
      //return 1;
    }else if ((pred.id > HashID) && ((arrHash[i] > pred.id) || (arrHash[i] <= HashID) ) ){ // Amogh - case added
      AddClientStoreStage7(arrHash[i], str);
      snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, arrHash[i], HashID); 
      logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
    }
    // couldn't store locally so...
    else
    {     
      if (FindSuccWithFTStage7(sock, arrHash[i], &TempB, foundStr) < 0){
        printf("projc client %s: HandleStoreMessage FindSuccWithFT fail!\n", Myname);
        return -1;
      }
    
      // We have targeted the destination, now ask it to store the content
      storeq.msgid = htonl(STORQ);
      storeq.ni = htonl(TempB.id);
      storeq.sl = htonl(nStrlen);
      memcpy(sendbuf, &storeq, sizeof(STQM));
      strncpy((sendbuf+sizeof(STQM)), str, nStrlen);
      nBytestosend = sizeof(STQM) + nStrlen;
      
      naaddr.sin_family = AF_INET;
      naaddr.sin_port = htons(TempB.port);
      naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
      
      if ((nSendbytes = sendto(sock, sendbuf, nBytestosend, 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != nBytestosend){
        printf("projc error: HandleStoreMsg sendto ret %d, should send %d\n", nSendbytes, nBytestosend);
        return -1;
      }
      
      LogTyiadMsg(STORQ, SENTFLAG, sendbuf);
      
      /**********************************************************************
      *** for stage >= 6, needs to judge if the hello messages comes here ******
      **********************************************************************/
      /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(recvbuf), 0, NULL, NULL)) < 0){
        printf("projc error: HandleStoreMsg recvfrom error.\n" );
        return -1;
      }*/

      if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) ) < 0) {
        printf("projc error: HandleUdpMessage GetNextMessage ret %d\n", nRecvbytes);
        return -1;
      }

      pstorer = (pstrm)recvbuf;
      if (ntohl(pstorer->msgid) != STORR || TempB.id != ntohl(pstorer->ni)){
        printf("projc error: HandleStoreMsg recvfrom sanity check fail!\n" );
        return -1;
      }
      
      ret = ntohl(pstorer->r);
      
      LogTyiadMsg(STORR, RECVFLAG, recvbuf);
      
      //log store succeed
      if (ret == 1){
        // just use sendbuf, as it's no more useful
        snprintf(sendbuf, sizeof(sendbuf), "add %s with hash 0x%08x to node 0x%08x\n", str, arrHash[i], TempB.id); 
        logfilewriteline(logfilename, sendbuf, strlen(sendbuf));
      } 
    } // end storq else if
  } // end for loop
  
  return ret;
}


int HandleSearchMsgStage7(int sock, char *str){
  unsigned int str_hash;
  TNode TempA, TempB;
  int flag = 0;
  char  szWritebuf[256];

  int ret;
  unsigned int arrHash[4], arrClose[4];
  int found = 0;
  int i = 0;
  char foundStr[MAX_TEXT_SIZE];
  unsigned int foundArr[4];
  int verified = 0;
  char copy[2][MAX_TEXT_SIZE];
  
  str_hash = gethashid(nMgrNonce, str);
  getHash(str_hash, arrHash);
  if (getClosestIDs(arrHash, arrClose) < 0) {
    printf("client %s error: Error finding out closest ids\n", Myname);
    return -1;
  }
    
  for (i = 0; i < 2; ++i) {
    
    printf("client %s search: searching for key: 0x%08x string: %s actual:0x%08x\n", Myname, arrClose[i], str, str_hash);

    if ( (arrClose[i] > pred.id && arrClose[i] <= HashID)  
        || (arrClose[i] < pred.id && pred.id > HashID && arrClose[i] <= HashID)
        || ((pred.id > HashID) && ((arrClose[i] > pred.id) || (arrClose[i] <= HashID) ) ) )

    {  // I should have it, do I?
      
      if ( ( (ret = SearchClientStoreStage7(arrClose[i], foundStr)) == 1) ) {
        printf("client %s: search %s with 0x%08x to node 0x%08x, key PRESENT\n", Myname, str, arrClose[i], HashID); 
        if (strncmp(str, foundStr, MAX_TEXT_SIZE) == 0)
        {
          verified++;
        }

        strncpy(copy[found], foundStr, MAX_TEXT_SIZE);
        foundArr[found] = HashID;
        ++found;
      }else{
        printf("client %s: search %s with 0x%08x to node 0x%08x, key ABSENT\n", Myname, str, arrClose[i], HashID); 
        
        snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, HashID); 
        logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
        return 0;
      }
      
    }else {
      if(arrClose[i] < HashID){   // start from predecessor
        //msgtype = ESTOQ;
        TempA.id = pred.id;
        TempA.port = pred.port;
      } else {
        TempA.id = succ.id;
        TempA.port = succ.port;
      }
      flag = 0;
      while(flag == 0){
        
        if (FindSuccWithFTStage7(sock, arrClose[i], &TempA, foundStr) < 0){
          printf("projc client %s: HandleStoreMessage FindSuccWithFT fail!\n", Myname);
          return -1;
        }

        if ((ret = FindClosestStage7(sock, ESTOQ, arrClose[i], TempA, &TempB, foundStr)) < 0){ // it's been changed to stores-q/r messages
          return -1;
        }
        
        if (TempA.id == TempB.id && ret == 1){  // find the node and it has id, yeah!
          printf("client %s: search %s with 0x%08x to node 0x%08x, key PRESENT\n", Myname, str, arrClose[i], TempA.id); 
          if (strncmp(str, foundStr, MAX_TEXT_SIZE) == 0)
          {
            ++verified;
          }
          strncpy(copy[found], foundStr, MAX_TEXT_SIZE);
          foundArr[found] = TempA.id;
          ++found;
          
          flag = 1;

        }else if(TempA.id == TempB.id && ret == 2){  // finde the node, but it doesn't have the id.
          printf("client %s: search %s with 0x%08x to node 0x%08x, key ABSENT\n", Myname, str, arrClose[i], TempA.id); 
          
          snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x, key ABSENT\n", str, TempA.id); 
          logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));
          return 0;

        }else{
          TempA.id = TempB.id;
          TempA.port = TempB.port;
        }
      }

    }
    
  } // end for loop


  if (found == 2) {
    printf("client %s: found %d times\n", Myname, found);
    if (strncmp(copy[0], copy[1], MAX_TEXT_SIZE) == 0)
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x and 0x%08x, key PRESENT and VERIFIED\n", str, foundArr[0], foundArr[1]); 
    else
      snprintf(szWritebuf, sizeof(szWritebuf), "search %s to node 0x%08x and 0x%08x, key PRESENT and DISAGREE\n", str, foundArr[0], foundArr[1]); 
    logfilewriteline(logfilename, szWritebuf, strlen(szWritebuf));

    return 1;
  } else {
    printf("client %s error: shouldn't reach here, string: %s found %d\n", Myname, str, found);
    return 0;
  }
  // should never get here
  return -1;
}

int inRightClosedRange(unsigned int val, unsigned int lb, unsigned int ub)
{
    if (lb < ub) return (lb < val && val <= ub);
    else if (lb > ub) return (lb < val || val <= ub);
    else return 0;  // lb == ub
}

int getClosestIDs(unsigned int a[4], unsigned int b[4])
{
  if (inRightClosedRange(HashID, a[0], a[1])) {
    b[0] = a[1]; b[1] = a[2]; b[2] = a[3]; b[3] = a[0];
  } 
  else if (inRightClosedRange(HashID, a[1], a[2])) {
    b[0] = a[2]; b[1] = a[3]; b[2] = a[0]; b[3] = a[1];
  }
  else if (inRightClosedRange(HashID, a[2], a[3])) {
    b[0] = a[3]; b[1] = a[0]; b[2] = a[1]; b[3] = a[2];
  }
  else if (inRightClosedRange(HashID, a[3], a[0])) {
    b[0] = a[0]; b[1] = a[1]; b[2] = a[2]; b[3] = a[3];
  }
  else {
    return -1;
  }
  return 0;
}

int FindSuccWithFTStage7(int sock, unsigned int id, TNode *retnode, char foundStr[MAX_TEXT_SIZE]){
  TNode tempA;
  TNode tempB;
  // start from first node
  tempA.id = HashIDfirst;
  tempA.port = FirstNodePort;
  
  //printf("%s: FindSuccWithFTStage7\n", Myname);
  // find first node successor.
  if (HashID == HashIDfirst){ // I am the first node
    tempB.id = succ.id;
    tempB.port = succ.port;
  }else{
    if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
      printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
      return -1;
    }
  }
  
  while(NotInRange(id, tempA.id, tempB.id)){
    // find closest 
    if (FindClosestStage7(sock, ESTOQ, id, tempA, &tempB, foundStr)<0){
      printf("projc client %s: FindSuccWithFT->FindClosest fail, tempA 0x%08x id 0x%08x \n", Myname, tempA.id, id);
      return -1;
    }

    // handle weired situation here
    // if A.id == B.id, there are two situation, 
    // 1. id belongs to A, 
    // 2. id belogns to A.successor
    // need some judgment here
    if (tempA.id == tempB.id){
      //find A's successor
      if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
        printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
        return -1;
      }

      if (tempA.id == tempB.id){  // first node, and we are the second node.
        retnode->id = tempA.id;
        retnode->port = tempA.port;
        return 0;
      }

      if (NotInRange(id, tempA.id, tempB.id)){ // A is successor
        retnode->id = tempA.id;
        retnode->port = tempA.port;
        return 0;
      }else{
        retnode->id = tempB.id;
        retnode->port  = tempB.port;
        return 0;
      }
    }

    tempA.id = tempB.id;
    tempA.port = tempB.port;

    //find A's successor
    if (FindNeighbor(sock, SUCCQ, tempA, &tempB) < 0){
      printf("projc client %s: FindSuccWithFT->FindNeighbor fail, tempA 0x%08x \n", Myname, tempA.id);
      return -1;
    }
  }

  retnode->id = tempB.id;
  retnode->port = tempB.port;
  
  return 0;
}


int FindClosestStage7(int sock, int msgt, unsigned int targetid, TNode na, TNode *pnb, char foundStr[MAX_TEXT_SIZE]){  // it's been changed to stores-q/r messages
  ESQM  qrymsg;
  pesrm premsg;
  struct sockaddr_in naaddr;
  char  sendbuf[128];
  char  recvbuf[128];
  int nSendbytes;
  int nRecvbytes;

  //printf("%s: FindClosestStage7\n", Myname );
  // make sure that I don't send message to myself
  if (na.id == HashID){
    // look up in my own finger table
    ClosestPrecedingFinger(targetid, pnb);
    return 0;
  }
  
  qrymsg.msgid = htonl(msgt);
  qrymsg.ni = htonl(na.id);
  qrymsg.di = htonl(targetid);
  memcpy(sendbuf, &qrymsg, sizeof(ESQM));
  
  naaddr.sin_family = AF_INET;
  naaddr.sin_port = htons(na.port);
  naaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
  if ((nSendbytes = sendto(sock, sendbuf, sizeof(ESQM), 0, (struct sockaddr *)&naaddr, sizeof(naaddr))) != sizeof(ESQM)){
    printf("projc error: find_closest sendto ret %d, should send %u\n", nSendbytes, sizeof(ESQM));
    return -1;
  }
  
  LogTyiadMsg(ESTOQ, SENTFLAG, sendbuf);
  
  // receive answer
  /**********************************************************************
  *** for stage >= 6, needs to judge if the hello messages comes here ******
  **********************************************************************/
  /*if ((nRecvbytes = recvfrom(sock, recvbuf, sizeof(CLRM), 0, NULL, NULL)) != sizeof(CLRM)){
    printf("projc error: find_closest recvfrom ret %d, shoulde recv %u\n", nRecvbytes, sizeof(CLRM));
    return -1;
  }*/
  if ((nRecvbytes = GetNextMessage(sock, recvbuf, sizeof(recvbuf)) )<= 0 ) {
    printf("projc %s error: FindClosestStage7 GetNextMessage ret %d\n", Myname, nRecvbytes);
    return -1;
  }
  
  premsg = (pesrm)recvbuf;
  
  if ((ESTOR) != ntohl(premsg->msgid) || na.id != ntohl(premsg->ni)){
    printf("projc error: find_closeststage7 recv ngr msg %d %d, ni %08x %08x\n", msgt, ntohl(premsg->msgid), na.id, ntohl(premsg->ni));
    return -1;
  }
  
  LogTyiadMsg(ESTOR, RECVFLAG, recvbuf);
  
  // following judgement is only used for search
  if (1 == ntohl(premsg->nhas) && na.id == ntohl(premsg->ri)){ // it has the string
    pnb->id = ntohl(premsg->ri);
    pnb->port = ntohl(premsg->rp);
    strncpy(foundStr, recvbuf+sizeof(ESRM), ntohl(premsg->sl));
    foundStr[ntohl(premsg->sl)] = '\0';

    return 1;
  }else if(0 == ntohl(premsg->nhas) && na.id == ntohl(premsg->ri)){ // it doesn't have the string
    pnb->id = ntohl(premsg->ri);
    pnb->port = ntohl(premsg->rp);
    return 2;
  }
  
  pnb->id = ntohl(premsg->ri);
  pnb->port = ntohl(premsg->rp);
  
  return 0;
}

